import { RPCCenter } from "../src/runtime/RPCCenter";
import { RPCRequest } from "../src/runtime/RPCRequest"
import { RPCHandle } from "../src/RPCHandle"
new RPCCenter(15001, "normal")

let request = new RPCRequest("test", 15001, "normal")
@RPCHandle.class("test", module)
class _ {
    @RPCHandle.init()
    init() {
        console.log("init")
    }

    @RPCHandle.route()
    @RPCHandle.paramRequired("age", "number", true)
    @RPCHandle.paramRequired("name", "string", true)
    async name(age: number, name: string) {

        throw { code: -1, errMsg: "你错误了", sds: 1111 }
    }
}

describe("nihao", function () {
    before(function () {
        return new Promise<void>(async function (reslove, reject) {
            try {
                await request.init()
                await RPCHandle.doInit("test", 15001)
                reslove()
            }
            catch (e) {
                reject()
            }
        })
    })

    it("name", function () {
        return new Promise<void>(function (resolve, reject) {
            request.Call("111", "name", "11", "qaweq").then(function (v) {
                console.log(v)
                resolve()
            }).catch(e => {
                reject(e)
            })
        })
    })

    after(function (done) {
        setTimeout(function () {
            process.exit()
        }, 100)
        done()
    })
})