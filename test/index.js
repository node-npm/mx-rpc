"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const RPCCenter_1 = require("../src/runtime/RPCCenter");
const RPCRequest_1 = require("../src/runtime/RPCRequest");
const RPCHandle_1 = require("../src/RPCHandle");
new RPCCenter_1.RPCCenter(15001, "normal");
let request = new RPCRequest_1.RPCRequest("test", 15001, "normal");
let _ = class _ {
    init() {
        console.log("init");
    }
    name(age, name) {
        return __awaiter(this, void 0, void 0, function* () {
            throw { code: -1, errMsg: "你错误了", sds: 1111 };
        });
    }
};
__decorate([
    RPCHandle_1.RPCHandle.init()
], _.prototype, "init", null);
__decorate([
    RPCHandle_1.RPCHandle.route(),
    RPCHandle_1.RPCHandle.paramRequired("age", "number", true),
    RPCHandle_1.RPCHandle.paramRequired("name", "string", true)
], _.prototype, "name", null);
_ = __decorate([
    RPCHandle_1.RPCHandle.class("test", module)
], _);
describe("nihao", function () {
    before(function () {
        return new Promise(function (reslove, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    yield request.init();
                    yield RPCHandle_1.RPCHandle.doInit("test", 15001);
                    reslove();
                }
                catch (e) {
                    reject();
                }
            });
        });
    });
    it("name", function () {
        return new Promise(function (resolve, reject) {
            request.Call("111", "name", "11", "qaweq").then(function (v) {
                console.log(v);
                resolve();
            }).catch(e => {
                reject(e);
            });
        });
    });
    after(function (done) {
        setTimeout(function () {
            process.exit();
        }, 100);
        done();
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHdEQUFxRDtBQUNyRCwwREFBc0Q7QUFDdEQsZ0RBQTRDO0FBQzVDLElBQUkscUJBQVMsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUE7QUFFOUIsSUFBSSxPQUFPLEdBQUcsSUFBSSx1QkFBVSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUE7QUFFckQsSUFBTSxDQUFDLEdBQVAsTUFBTSxDQUFDO0lBRUgsSUFBSTtRQUNBLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7SUFDdkIsQ0FBQztJQUtLLElBQUksQ0FBQyxHQUFXLEVBQUUsSUFBWTs7WUFFaEMsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQTtRQUNqRCxDQUFDO0tBQUE7Q0FDSixDQUFBO0FBWEc7SUFEQyxxQkFBUyxDQUFDLElBQUksRUFBRTs2QkFHaEI7QUFLRDtJQUhDLHFCQUFTLENBQUMsS0FBSyxFQUFFO0lBQ2pCLHFCQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDO0lBQzlDLHFCQUFTLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDOzZCQUkvQztBQVpDLENBQUM7SUFETixxQkFBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDO0dBQzFCLENBQUMsQ0FhTjtBQUVELFFBQVEsQ0FBQyxPQUFPLEVBQUU7SUFDZCxNQUFNLENBQUM7UUFDSCxPQUFPLElBQUksT0FBTyxDQUFPLFVBQWdCLE9BQU8sRUFBRSxNQUFNOztnQkFDcEQsSUFBSTtvQkFDQSxNQUFNLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtvQkFDcEIsTUFBTSxxQkFBUyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUE7b0JBQ3JDLE9BQU8sRUFBRSxDQUFBO2lCQUNaO2dCQUNELE9BQU8sQ0FBQyxFQUFFO29CQUNOLE1BQU0sRUFBRSxDQUFBO2lCQUNYO1lBQ0wsQ0FBQztTQUFBLENBQUMsQ0FBQTtJQUNOLENBQUMsQ0FBQyxDQUFBO0lBRUYsRUFBRSxDQUFDLE1BQU0sRUFBRTtRQUNQLE9BQU8sSUFBSSxPQUFPLENBQU8sVUFBVSxPQUFPLEVBQUUsTUFBTTtZQUM5QyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7Z0JBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7Z0JBQ2QsT0FBTyxFQUFFLENBQUE7WUFDYixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1QsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ2IsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUMsQ0FBQyxDQUFBO0lBRUYsS0FBSyxDQUFDLFVBQVUsSUFBSTtRQUNoQixVQUFVLENBQUM7WUFDUCxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDbEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBQ1AsSUFBSSxFQUFFLENBQUE7SUFDVixDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUMsQ0FBQyxDQUFBIn0=