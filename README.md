# mx-rpc

#### 介绍
{提供一个通过socket.io为通信方式的rpc调用机制}

#### 软件架构
软件架构说明


#### 安装教程



#### 使用说明
```
import { RPCCenter,RPCRequest,RPCHandle } from "mx-rpc";
new RPCCenter(15001)

let request = new RPCRequest("test", 15001)
@RPCHandle.class("test", module)
class _ {
    @RPCHandle.init()
    init() {
        console.log("init")
    }

    @RPCHandle.route()
    @RPCHandle.paramRequired("age", "number", true)
    @RPCHandle.paramRequired("name", "string", true)
    async name(age: number, name: string) {
        console.log(age, typeof age)
        return "nihao"
    }
}

async function start(){
    await request.init();
    await RPCHandle.doInit("test", 15001);
    let result = await request.Call("111", "name","11","qaweq");
    console.log(result);
}

```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request