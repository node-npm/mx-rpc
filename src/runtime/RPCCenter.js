"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RPCCenter = void 0;
const socket_io_1 = require("../net/socket.io");
const proto_1 = require("../proto");
const hashring_1 = __importDefault(require("hashring"));
const zlib_1 = require("zlib");
// export class DispatchModule {
//     static dispatchMap: { [name: string]: (pool: string[], reqid: string) => string } = {};
//     static dispatchMethod: string = "";
//     static registDispatchMethod(name: string, fn: (pool: string[], reqid: string) => string) {
//         this.dispatchMap[name] = fn;
//     }
//     static switchMethod(name: string) {
//         this.dispatchMethod = name;
//     }
//     static getDispatch(name?: string) {
//         if (!name) name = this.dispatchMethod;
//         if (!name) return null;
//         if (!this.dispatchMap.hasOwnProperty(name)) return null;
//         return this.dispatchMap[name];
//     }
// }
class HashringModule {
    constructor(...ids) {
        if (ids.length > 0) {
            this.roles = [...ids];
            this.inst = new hashring_1.default(ids);
        }
        else {
            this.roles = [];
            this.inst = new hashring_1.default([]);
        }
    }
    empty() {
        return this.roles.length == 0;
    }
    add(id, weight) {
        if (weight) {
            this.inst.add({ id: { weight: weight } });
        }
        else {
            this.inst.add(id);
        }
        this.roles.push(id);
    }
    remove(id) {
        this.inst.remove(id);
        let idx = this.roles.indexOf(id);
        if (idx >= 0) {
            this.roles.splice(idx, 1);
        }
    }
    dispatch(key) {
        if (this.roles.length == 1)
            return this.roles[0];
        let mq = key.split('.')[0];
        if (!mq) {
            // 如果没有指定位置那么就随机
            // let idx = Math.floor(Math.random() * this.roles.length)
            // return this.roles[idx]
            // 随机就直接走一致性hash好了
            return this.inst.get(key);
        }
        else {
            return this.inst.get(mq);
        }
    }
    foreach(callbackfn) {
        this.roles.forEach(callbackfn);
    }
}
// 负责转发RPC相关的消息
class RPCCenter {
    constructor(srv, compress) {
        // inside: InsideClient = gInsideClient;
        this.openCompress = "normal";
        this.centerSign = proto_1.gSystemSign;
        this.req2sockets = new Map;
        this.role2sockets = new Map;
        this.sockets2role = new Map;
        if (typeof srv == "number") {
            this.net = new socket_io_1.RPCNetServer(srv);
        }
        else if (srv instanceof socket_io_1.RPCNetServer) {
            this.net = srv;
        }
        else {
            this.net = new socket_io_1.RPCNetServer();
        }
        // 压缩类型
        this.openCompress = compress || "";
        this.net.on("close", this.onExist.bind(this));
        this.net.on("REGIST", this.onRegist.bind(this));
        // 请求
        this.net.on("RPC", this.onRPC.bind(this, ""));
        this.net.on("RPC_C", (socketid, reqid, route, ...args) => {
            let compressMeth = "";
            let unit = this.sockets2role.get(socketid);
            if (unit) {
                compressMeth = unit.compress;
            }
            this.onRPC(compressMeth, socketid, reqid, route, ...args);
        });
        // 应答
        this.net.on("RPCRET", this.onRPCRET.bind(this, ""));
        this.net.on("RPCRET_C", (socketid, reqid, route, ...args) => {
            let compressMeth = "";
            let unit = this.sockets2role.get(socketid);
            if (unit) {
                compressMeth = unit.compress;
            }
            this.onRPCRET(compressMeth, socketid, reqid, route, ...args);
        });
        // 广播
        this.net.on("RPCBC", this.onRPCBC.bind(this, ""));
        this.net.on("RPCBC_C", (socketid, reqid, toSelf, route, ...args) => {
            let compressMeth = "";
            let unit = this.sockets2role.get(socketid);
            if (unit) {
                compressMeth = unit.compress;
            }
            this.onRPCBC(compressMeth, socketid, reqid, toSelf, route, ...args);
        });
        // this.inside.on("RPC", this.onRPC.bind(this))
        // this.inside.on("RPCRET", this.onRPCRET.bind(this))
        // this.inside.on("RPCBC", this.onRPCBC.bind(this))
    }
    send(compress, socketid, method, reqid, route, ...args) {
        if (!compress) {
            return this.net.send(socketid, method, reqid, route, ...args);
        }
        // 如果数据是压缩过的
        let role = this.sockets2role.get(socketid);
        if ((role === null || role === void 0 ? void 0 : role.compress) != "") {
            return this.net.send(socketid, method, reqid, route, ...args);
        }
        // 那么这里需要解压后提供 向下兼容比较费效率一般不要出现
        // 这里需要解压内容，
        zlib_1.inflateRaw(Buffer.from(args[0]), (e, result) => {
            if (e) {
                this.net.send(socketid, method, reqid, route, ...args);
            }
            else {
                try {
                    let info = JSON.parse(result.toString());
                    // 这里成功兼容后需要处理一下信号的转换
                    this.net.send(socketid, method.replace("_C", ""), reqid, route, ...info);
                }
                catch (e) {
                    this.net.send(socketid, method, reqid, route, ...args);
                }
            }
        });
    }
    onRegist(socketid, role, isRequest, clientSign, compress) {
        if (!isRequest) {
            let sockPool = this.role2sockets.get(role);
            if (!sockPool) {
                this.role2sockets.set(role, new HashringModule(socketid));
            }
            else {
                sockPool.add(socketid);
            }
        }
        // 回复一个事件
        this.send(false, socketid, "REGISTRET", this.centerSign, this.openCompress);
        let rolesPool = this.sockets2role.get(socketid);
        if (!rolesPool) {
            rolesPool = { roles: {}, sign: clientSign, compress: compress || "" };
            this.sockets2role.set(socketid, rolesPool);
        }
        if (!rolesPool.roles[role]) {
            rolesPool.roles[role] = {
                requestCount: 0,
                handleCount: 0,
            };
        }
        if (isRequest)
            rolesPool.roles[role].requestCount++;
        else
            rolesPool.roles[role].handleCount++;
    }
    onExist(socketid) {
        let role = this.sockets2role.get(socketid);
        if (!role) {
            // 正常不存在这个情况
            return;
        }
        this.sockets2role.delete(socketid);
        for (let roleName in role.roles) {
            let rolePool = this.role2sockets.get(roleName);
            if (!rolePool) {
                // 正常不存在这个情况
                continue;
            }
            rolePool.remove(socketid);
        }
    }
    onRPC(compress, socketid, reqid, route, ...args) {
        // 这里route错了需要调整
        if (route == undefined || route == null || typeof route != "string") {
            console.error(...arguments);
            return;
        }
        let rouets = route.split('.');
        // 这里根据routehead 找到需要的角色
        let rolePool = this.role2sockets.get(rouets[0]);
        if (!rolePool || rolePool.empty()) {
            // 表示不存在，那么直接回复404
            this.send(false, socketid, "RPCRET", reqid, route, 404);
            return;
        }
        // 收到rpc请求，这里把requestid和socketid绑定一下
        this.req2sockets.set(reqid, socketid);
        // let dispatch = DispatchModule.getDispatch() || this.dispatch;
        let chooseId = "";
        if (this.dispatch) {
            chooseId = this.dispatch(rolePool.roles, reqid, route);
        }
        else {
            chooseId = rolePool.dispatch(reqid);
        }
        this.send(compress != "", chooseId, compress == "" ? "RPC" : "RPC_C", reqid, route, ...args);
    }
    onRPCRET(compress, socketid, reqid, route, ...args) {
        // 收到rpc请求，这里把requestid和socketid绑定一下
        let sid = this.req2sockets.get(reqid);
        if (!sid) {
            // 不存在这种情况，除非连接断开了，或者进程中断过 后续考虑是否使用消息队列或者其它高级方式
            return;
        }
        this.req2sockets.delete(reqid);
        this.send(compress != "", sid, compress == "" ? "RPCRET" : "RPCRET_C", reqid, route, ...args);
    }
    onRPCBC(compress, socketid, reqid, toSelf, route, ...args) {
        // 这里route错了需要调整
        if (route == undefined || route == null || typeof route != "string") {
            console.error(...arguments);
            return;
        }
        let rouets = route.split('.');
        // 这里根据routehead 找到需要的角色
        let rolePool = this.role2sockets.get(rouets[0]);
        if (!rolePool || rolePool.empty())
            return;
        // 收到rpc请求，这里把requestid和socketid绑定一下
        rolePool.foreach((sid) => {
            if (!toSelf && sid == socketid)
                return;
            this.send(compress != "", sid, compress == "" ? "RPCBC" : "RPCBC_C", reqid, route, ...args);
        });
        // for (let i = 0; i < rolePools.length; i++) {
        //     let sid = rolePools[i];
        //     if (!toSelf && sid == socketid) {
        //         continue;
        //     }
        //     this.send(sid, "RPCBC", reqid, route, ...args)
        // }
    }
}
exports.RPCCenter = RPCCenter;
// export async function test() {
//     let s = new RPCCenter()
//     let rh = new RPCHandle("name")
//     let rc = new RPCRequest("name")
//     rh.Listen("getname", function () {
//         return "chenkai"
//     })
//     let result = await rc.Call("getname", "chenkai")
//     console.log(result)
// }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUlBDQ2VudGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUlBDQ2VudGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLGdEQUFnRDtBQUNoRCxvQ0FBcUQ7QUFDckQsd0RBQStCO0FBQy9CLCtCQUFrQztBQUNsQyxnQ0FBZ0M7QUFDaEMsOEZBQThGO0FBQzlGLDBDQUEwQztBQUMxQyxpR0FBaUc7QUFDakcsdUNBQXVDO0FBQ3ZDLFFBQVE7QUFFUiwwQ0FBMEM7QUFDMUMsc0NBQXNDO0FBQ3RDLFFBQVE7QUFFUiwwQ0FBMEM7QUFDMUMsaURBQWlEO0FBQ2pELGtDQUFrQztBQUNsQyxtRUFBbUU7QUFFbkUseUNBQXlDO0FBQ3pDLFFBQVE7QUFDUixJQUFJO0FBRUosTUFBTSxjQUFjO0lBR2hCLFlBQVksR0FBRyxHQUFhO1FBQ3hCLElBQUksR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLGtCQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDakM7YUFDSTtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxrQkFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2hDO0lBQ0wsQ0FBQztJQUVELEtBQUs7UUFDRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsR0FBRyxDQUFDLEVBQVUsRUFBRSxNQUFlO1FBQzNCLElBQUksTUFBTSxFQUFFO1lBQ1IsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQzdDO2FBQ0k7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNyQjtRQUVELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxNQUFNLENBQUMsRUFBVTtRQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3JCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtZQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQTtTQUM1QjtJQUNMLENBQUM7SUFFRCxRQUFRLENBQUMsR0FBVztRQUNoQixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUM7WUFBRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDaEQsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUMxQixJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ0wsZ0JBQWdCO1lBQ2hCLDBEQUEwRDtZQUMxRCx5QkFBeUI7WUFDekIsa0JBQWtCO1lBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7U0FDNUI7YUFDSTtZQUNELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUI7SUFDTCxDQUFDO0lBRUQsT0FBTyxDQUFDLFVBQW1FO1FBQ3ZFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Q0FDSjtBQUdELGVBQWU7QUFDZixNQUFhLFNBQVM7SUFTbEIsWUFBWSxHQUEyQixFQUFFLFFBQXVCO1FBUGhFLHdDQUF3QztRQUN4QyxpQkFBWSxHQUFpQixRQUFRLENBQUE7UUFDckMsZUFBVSxHQUFHLG1CQUFXLENBQUM7UUFDekIsZ0JBQVcsR0FBd0IsSUFBSSxHQUFHLENBQUM7UUFDM0MsaUJBQVksR0FBZ0MsSUFBSSxHQUFHLENBQUM7UUFDcEQsaUJBQVksR0FBOEgsSUFBSSxHQUFHLENBQUM7UUFHOUksSUFBSSxPQUFPLEdBQUcsSUFBSSxRQUFRLEVBQUU7WUFDeEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLHdCQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDcEM7YUFDSSxJQUFJLEdBQUcsWUFBWSx3QkFBWSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1NBQ2xCO2FBQ0k7WUFDRCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksd0JBQVksRUFBRSxDQUFDO1NBQ2pDO1FBRUQsT0FBTztRQUNQLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxJQUFJLEVBQUUsQ0FBQTtRQUVsQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUMvQyxLQUFLO1FBQ0wsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBQzdDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQWdCLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxHQUFHLElBQVcsRUFBRSxFQUFFO1lBQ3BGLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQTtZQUNyQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzQyxJQUFJLElBQUksRUFBRTtnQkFDTixZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUNoQztZQUVELElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUE7UUFDN0QsQ0FBQyxDQUFDLENBQUE7UUFFRixLQUFLO1FBQ0wsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBQ25ELElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQWdCLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxHQUFHLElBQVcsRUFBRSxFQUFFO1lBQ3ZGLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQTtZQUNyQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMzQyxJQUFJLElBQUksRUFBRTtnQkFDTixZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUNoQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUE7UUFDaEUsQ0FBQyxDQUFDLENBQUE7UUFDRixLQUFLO1FBQ0wsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBQ2pELElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDLFFBQWdCLEVBQUUsS0FBYSxFQUFFLE1BQWUsRUFBRSxLQUFhLEVBQUUsR0FBRyxJQUFXLEVBQUUsRUFBRTtZQUN2RyxJQUFJLFlBQVksR0FBRyxFQUFFLENBQUE7WUFDckIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0MsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDaEM7WUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQTtRQUN2RSxDQUFDLENBQUMsQ0FBQTtRQUVGLCtDQUErQztRQUMvQyxxREFBcUQ7UUFDckQsbURBQW1EO0lBQ3ZELENBQUM7SUFFUyxJQUFJLENBQUMsUUFBaUIsRUFBRSxRQUFnQixFQUFFLE1BQWMsRUFBRSxLQUFhLEVBQUUsS0FBYSxFQUFFLEdBQUcsSUFBVztRQUM1RyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ1gsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQztTQUNqRTtRQUNELFlBQVk7UUFDWixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUEsSUFBSSxhQUFKLElBQUksdUJBQUosSUFBSSxDQUFFLFFBQVEsS0FBSSxFQUFFLEVBQUU7WUFDdEIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQTtTQUNoRTtRQUVELDhCQUE4QjtRQUM5QixZQUFZO1FBQ1osaUJBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzNDLElBQUksQ0FBQyxFQUFFO2dCQUNILElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFBO2FBQ3pEO2lCQUNJO2dCQUNELElBQUk7b0JBQ0EsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQTtvQkFDeEMscUJBQXFCO29CQUNyQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFBO2lCQUMzRTtnQkFDRCxPQUFPLENBQUMsRUFBRTtvQkFDTixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQTtpQkFDekQ7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVPLFFBQVEsQ0FBQyxRQUFnQixFQUFFLElBQVksRUFBRSxTQUFrQixFQUFFLFVBQWtCLEVBQUUsUUFBZ0I7UUFDckcsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNaLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQzFDLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ1gsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLElBQUksY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7YUFDNUQ7aUJBQ0k7Z0JBQ0QsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTthQUN6QjtTQUNKO1FBQ0QsU0FBUztRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDM0UsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNaLFNBQVMsR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsUUFBUSxJQUFJLEVBQUUsRUFBRSxDQUFBO1lBQ3JFLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztTQUM5QztRQUVELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3hCLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUc7Z0JBQ3BCLFlBQVksRUFBRSxDQUFDO2dCQUNmLFdBQVcsRUFBRSxDQUFDO2FBQ2pCLENBQUE7U0FDSjtRQUVELElBQUksU0FBUztZQUNULFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsWUFBWSxFQUFFLENBQUM7O1lBRXJDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDNUMsQ0FBQztJQUVPLE9BQU8sQ0FBQyxRQUFnQjtRQUM1QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUMxQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1AsWUFBWTtZQUNaLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRW5DLEtBQUssSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUM3QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNYLFlBQVk7Z0JBQ1osU0FBUTthQUNYO1lBRUQsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM3QjtJQUNMLENBQUM7SUFLTyxLQUFLLENBQUMsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLEtBQWEsRUFBRSxLQUFhLEVBQUUsR0FBRyxJQUFXO1FBQzFGLGdCQUFnQjtRQUNoQixJQUFJLEtBQUssSUFBSSxTQUFTLElBQUksS0FBSyxJQUFJLElBQUksSUFBSSxPQUFPLEtBQUssSUFBSSxRQUFRLEVBQUU7WUFDakUsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDO1lBQzVCLE9BQU87U0FDVjtRQUVELElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUIsd0JBQXdCO1FBQ3hCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQy9DLElBQUksQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQy9CLGtCQUFrQjtZQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUE7WUFDdkQsT0FBTztTQUNWO1FBRUQsb0NBQW9DO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQTtRQUNyQyxnRUFBZ0U7UUFDaEUsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNmLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFBO1NBQ3pEO2FBQ0k7WUFDRCxRQUFRLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN2QztRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLEVBQUUsRUFBRSxRQUFRLEVBQUUsUUFBUSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFBO0lBQ2hHLENBQUM7SUFFTyxRQUFRLENBQUMsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLEtBQWEsRUFBRSxLQUFhLEVBQUUsR0FBRyxJQUFXO1FBQzdGLG9DQUFvQztRQUNwQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNyQyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ04sK0NBQStDO1lBQy9DLE9BQU07U0FDVDtRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsUUFBUSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFBO0lBQ2pHLENBQUM7SUFFTyxPQUFPLENBQUMsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLEtBQWEsRUFBRSxNQUFlLEVBQUUsS0FBYSxFQUFFLEdBQUcsSUFBVztRQUM3RyxnQkFBZ0I7UUFDaEIsSUFBSSxLQUFLLElBQUksU0FBUyxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksT0FBTyxLQUFLLElBQUksUUFBUSxFQUFFO1lBQ2pFLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQztZQUM1QixPQUFPO1NBQ1Y7UUFFRCxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLHdCQUF3QjtRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUMvQyxJQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLEVBQUU7WUFBRSxPQUFPO1FBRTFDLG9DQUFvQztRQUNwQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDckIsSUFBSSxDQUFDLE1BQU0sSUFBSSxHQUFHLElBQUksUUFBUTtnQkFBRSxPQUFPO1lBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsUUFBUSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFBO1FBQy9GLENBQUMsQ0FBQyxDQUFDO1FBQ0gsK0NBQStDO1FBQy9DLDhCQUE4QjtRQUM5Qix3Q0FBd0M7UUFDeEMsb0JBQW9CO1FBQ3BCLFFBQVE7UUFDUixxREFBcUQ7UUFDckQsSUFBSTtJQUNSLENBQUM7Q0FDSjtBQXRORCw4QkFzTkM7QUFHRCxpQ0FBaUM7QUFDakMsOEJBQThCO0FBQzlCLHFDQUFxQztBQUNyQyxzQ0FBc0M7QUFDdEMseUNBQXlDO0FBQ3pDLDJCQUEyQjtBQUMzQixTQUFTO0FBQ1QsdURBQXVEO0FBRXZELDBCQUEwQjtBQUMxQixJQUFJIn0=