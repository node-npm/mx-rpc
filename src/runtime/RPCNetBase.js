"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RPCNetBase = void 0;
const socket_io_1 = require("../net/socket.io");
const events_1 = require("events");
const proto_1 = require("../proto");
class RPCNetBase extends events_1.EventEmitter {
    constructor(role, req, srv, compress) {
        super();
        // inside: InsideClient = gInsideClient;
        this.clientSign = proto_1.gSystemSign;
        this.centerSign = "";
        this.compress = "";
        this.openCompress = false;
        this.onConnect = (function () {
            process.nextTick(this.send.bind(this), "REGIST", this.role, this.isReq, this.clientSign, this.compress);
        }).bind(this);
        this.isReq = req;
        this.role = role;
        if (typeof srv == "number") {
            this.net = new socket_io_1.RPCNetClient(srv);
        }
        else if (srv instanceof socket_io_1.RPCNetClient) {
            this.net = srv;
        }
        else {
            this.net = new socket_io_1.RPCNetClient();
        }
        this.compress = compress || "normal";
        this.net.on("data", this.onData.bind(this));
        this.net.on("disconnect", this._disconnect.bind(this));
        this.onConnect();
    }
    _disconnect() {
        // 这里标识网络断开了
        this.net.removeListener("connection", this.onConnect);
        this.net.once("connection", this.onConnect);
    }
    _untilConnected(r, j) {
        if (this.net.connected || this.net.connectedF) {
            if (this.net.connected)
                r(true);
            else
                j();
        }
        else
            setTimeout(this._untilConnected.bind(this, r, j), 10);
    }
    init() {
        return new Promise((resolve, reject) => {
            this._untilConnected(resolve, reject);
        });
    }
    registret(sign, compress) {
        this.centerSign = sign;
        if (this.compress.length > 0 && this.compress == compress) {
            this.openCompress = true;
        }
    }
    send(...args) {
        // if (this.clientSign == this.centerSign) {
        //     return this.inside.send(...args);
        // }
        this.net.send(...args);
    }
    onData(type, requestID, route, ...args) {
        if (type == "REGISTRET") {
            this.registret(requestID, route);
            return;
        }
        // 这里route错了需要调整
        if (route == undefined || route == null || typeof route != "string") {
            console.error(...arguments);
            return;
        }
        let routes = route.split('.');
        // 确定一下角色正确性
        if (this.role != routes[0])
            return;
        if (this.listenerCount(type) != 0) {
            let content = {
                requestID: requestID,
                type: type,
                route: routes.slice(1, routes.length).join('.'),
                args: args,
            };
            this.emit(type, content);
        }
    }
}
exports.RPCNetBase = RPCNetBase;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUlBDTmV0QmFzZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlJQQ05ldEJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsZ0RBQWdEO0FBQ2hELG1DQUFzQztBQUN0QyxvQ0FBaUU7QUFFakUsTUFBYSxVQUFXLFNBQVEscUJBQVk7SUFTeEMsWUFBWSxJQUFZLEVBQUUsR0FBWSxFQUFFLEdBQTJCLEVBQUUsUUFBdUI7UUFDeEYsS0FBSyxFQUFFLENBQUM7UUFSWix3Q0FBd0M7UUFDeEMsZUFBVSxHQUFHLG1CQUFXLENBQUM7UUFDekIsZUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNoQixhQUFRLEdBQWlCLEVBQUUsQ0FBQztRQUM1QixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQTZCdEIsY0FBUyxHQUFHLENBQUM7WUFDakIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQTFCVCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLE9BQU8sR0FBRyxJQUFJLFFBQVEsRUFBRTtZQUN4QixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksd0JBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUVwQzthQUNJLElBQUksR0FBRyxZQUFZLHdCQUFZLEVBQUU7WUFDbEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7U0FDbEI7YUFDSTtZQUNELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSx3QkFBWSxFQUFFLENBQUM7U0FDakM7UUFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsSUFBSSxRQUFRLENBQUM7UUFDckMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7UUFDdEQsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFTyxXQUFXO1FBQ2YsWUFBWTtRQUNaLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDckQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtJQUMvQyxDQUFDO0lBTU8sZUFBZSxDQUFDLENBQXNCLEVBQUUsQ0FBWTtRQUN4RCxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFO1lBQzNDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTO2dCQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQTs7Z0JBQzFCLENBQUMsRUFBRSxDQUFBO1NBQ1g7O1lBQ0ksVUFBVSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUE7SUFDOUQsQ0FBQztJQUVELElBQUk7UUFDQSxPQUFPLElBQUksT0FBTyxDQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzVDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFBO1FBQ3pDLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVPLFNBQVMsQ0FBQyxJQUFZLEVBQUUsUUFBc0I7UUFDbEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxRQUFRLEVBQUU7WUFDdkQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDNUI7SUFDTCxDQUFDO0lBQ0QsSUFBSSxDQUFDLEdBQUcsSUFBVztRQUNmLDRDQUE0QztRQUM1Qyx3Q0FBd0M7UUFDeEMsSUFBSTtRQUNKLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUNPLE1BQU0sQ0FBQyxJQUFZLEVBQUUsU0FBaUIsRUFBRSxLQUFhLEVBQUUsR0FBRyxJQUFXO1FBQ3pFLElBQUksSUFBSSxJQUFJLFdBQVcsRUFBRTtZQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFZLENBQUMsQ0FBQztZQUN4QyxPQUFPO1NBQ1Y7UUFFRCxnQkFBZ0I7UUFDaEIsSUFBSSxLQUFLLElBQUksU0FBUyxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksT0FBTyxLQUFLLElBQUksUUFBUSxFQUFFO1lBQ2pFLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQztZQUM1QixPQUFPO1NBQ1Y7UUFFRCxJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLFlBQVk7UUFDWixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN0QixPQUFPO1FBQ1gsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUMvQixJQUFJLE9BQU8sR0FBZTtnQkFDdEIsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLElBQUksRUFBRSxJQUFJO2dCQUNWLEtBQUssRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztnQkFDL0MsSUFBSSxFQUFFLElBQUk7YUFDYixDQUFDO1lBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7U0FDNUI7SUFDTCxDQUFDO0NBQ0o7QUEzRkQsZ0NBMkZDIn0=