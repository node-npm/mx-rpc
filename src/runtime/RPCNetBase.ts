import { RPCNetClient } from "../net/socket.io";
import { EventEmitter } from "events";
import { RPCContent, gSystemSign, CompressType } from "../proto";
import { gInsideClient, InsideClient } from "../net/InsideClient";
export class RPCNetBase extends EventEmitter {
    net: RPCNetClient;
    // inside: InsideClient = gInsideClient;
    clientSign = gSystemSign;
    centerSign = "";
    compress: CompressType = "";
    openCompress: boolean = false;
    role: string;
    isReq: boolean;
    constructor(role: string, req: boolean, srv?: number | RPCNetClient, compress?: CompressType) {
        super();
        this.isReq = req;
        this.role = role;
        if (typeof srv == "number") {
            this.net = new RPCNetClient(srv);

        }
        else if (srv instanceof RPCNetClient) {
            this.net = srv;
        }
        else {
            this.net = new RPCNetClient();
        }
        this.compress = compress || "normal";
        this.net.on("data", this.onData.bind(this));
        this.net.on("disconnect", this._disconnect.bind(this))
        this.onConnect();
    }

    private _disconnect() {
        // 这里标识网络断开了
        this.net.removeListener("connection", this.onConnect)
        this.net.once("connection", this.onConnect)
    }

    private onConnect = (function (this: RPCNetBase) {
        process.nextTick(this.send.bind(this), "REGIST", this.role, this.isReq, this.clientSign, this.compress);
    }).bind(this)

    private _untilConnected(r: (b: boolean) => any, j: () => any) {
        if (this.net.connected || this.net.connectedF) {
            if (this.net.connected) r(true)
            else j()
        }
        else setTimeout(this._untilConnected.bind(this, r, j), 10)
    }

    init() {
        return new Promise<boolean>((resolve, reject) => {
            this._untilConnected(resolve, reject)
        })
    }

    private registret(sign: string, compress: CompressType) {
        this.centerSign = sign;
        if (this.compress.length > 0 && this.compress == compress) {
            this.openCompress = true;
        }
    }
    send(...args: any[]) {
        // if (this.clientSign == this.centerSign) {
        //     return this.inside.send(...args);
        // }
        this.net.send(...args);
    }
    private onData(type: string, requestID: string, route: string, ...args: any[]) {
        if (type == "REGISTRET") {
            this.registret(requestID, route as any);
            return;
        }

        // 这里route错了需要调整
        if (route == undefined || route == null || typeof route != "string") {
            console.error(...arguments);
            return;
        }

        let routes = route.split('.');
        // 确定一下角色正确性
        if (this.role != routes[0])
            return;
        if (this.listenerCount(type) != 0) {
            let content: RPCContent = {
                requestID: requestID,
                type: type,
                route: routes.slice(1, routes.length).join('.'),
                args: args,
            };
            this.emit(type, content);
        }
    }
}
