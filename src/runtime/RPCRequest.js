"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RPCRequest = void 0;
const zlib_1 = require("zlib");
const RPCNetBase_1 = require("./RPCNetBase");
// 这里提供rpc接口服务
class RPCRequest extends RPCNetBase_1.RPCNetBase {
    constructor(role, srv, compress) {
        super(role, true, srv, compress);
        this.genRPC = 0;
        this.mapRPCMap = new Map;
        this.on("RPCRET", this.onRPCBack.bind(this));
        this.on("RPCRET_C", (context) => {
            let bf = Buffer.from(context.args[0]);
            zlib_1.inflateRaw(bf, (e, result) => {
                if (e) {
                    this.onRPCBack(context);
                    return;
                }
                try {
                    context.args = JSON.parse(result.toString());
                }
                catch (e) {
                }
                this.onRPCBack(context);
            });
        });
    }
    makeRequestID(role) {
        if (this.genRPC == Number.MAX_SAFE_INTEGER) {
            // 防止溢出，这里增加一个判断
            this.genRPC = 0;
        }
        let id = "R" + role + "P" + process.pid + "I" + this.genRPC++;
        return id;
    }
    onRPCBack(context) {
        let code = context.args[0];
        let callP = this.mapRPCMap.get(context.requestID);
        if (!callP)
            return;
        // 移除请求
        this.mapRPCMap.delete(context.requestID);
        if (code == 0) {
            callP.reslove(context.args[1]);
        }
        else {
            try {
                callP.reject(JSON.parse(context.args[1]));
            }
            catch (e) {
                callP.reject({ code: context.args[0], errMsg: context.args[1] });
            }
        }
    }
    /**
     * 提供rpc的调用接口
     * @param dispatchKey 分发时的识别控制id
     * @param route 请求路径
     * @param args 请求参数
     */
    Call(dispatchKey, route, ...args) {
        return new Promise((reslove, reject) => {
            let reqId = dispatchKey + '.' + this.makeRequestID(this.role);
            if (this.openCompress) {
                // 这里需要针对数据压缩处理一下
                let cm = JSON.stringify(args);
                // 调用压缩算法
                zlib_1.deflateRaw(cm, (e, result) => {
                    if (e) {
                        this.send("RPC", reqId, [this.role, route].join("."), ...args);
                    }
                    else {
                        this.send("RPC_C", reqId, [this.role, route].join("."), result);
                    }
                });
            }
            else {
                this.send("RPC", reqId, [this.role, route].join("."), ...args);
            }
            this.mapRPCMap.set(reqId, { reslove, reject });
        });
    }
    /**
     * 广播调用，这种就不需要统计返回结果了
     * @param toSelf 是否需要抄送自己
     * @param route
     * @param args
     */
    CallBroadcast(toSelf, route, ...args) {
        let reqId = this.makeRequestID(this.role);
        if (this.openCompress) {
            // 这里需要针对数据压缩处理一下
            let cm = JSON.stringify(args);
            // 调用压缩算法
            zlib_1.deflateRaw(cm, (e, result) => {
                if (e) {
                    this.send("RPCBC", reqId, toSelf, [this.role, route].join("."), ...args);
                }
                else {
                    this.send("RPCBC_C", reqId, toSelf, [this.role, route].join("."), result);
                }
            });
        }
        else {
            this.send("RPCBC", reqId, toSelf, [this.role, route].join("."), ...args);
        }
    }
}
exports.RPCRequest = RPCRequest;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUlBDUmVxdWVzdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIlJQQ1JlcXVlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsK0JBQThDO0FBRzlDLDZDQUEwQztBQUMxQyxjQUFjO0FBQ2QsTUFBYSxVQUFXLFNBQVEsdUJBQVU7SUFjdEMsWUFBWSxJQUFZLEVBQUUsR0FBMkIsRUFBRSxRQUF1QjtRQUMxRSxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFkckMsV0FBTSxHQUFHLENBQUMsQ0FBQztRQVNYLGNBQVMsR0FHSixJQUFJLEdBQUcsQ0FBQztRQUdULElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFtQixFQUFFLEVBQUU7WUFDeEMsSUFBSSxFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsaUJBQVUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxFQUFFO29CQUNILElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7b0JBQ3ZCLE9BQU07aUJBQ1Q7Z0JBRUQsSUFBSTtvQkFDQSxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7aUJBQ2hEO2dCQUNELE9BQU8sQ0FBQyxFQUFFO2lCQUNUO2dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDM0IsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUEvQlMsYUFBYSxDQUFDLElBQVk7UUFDaEMsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtZQUN4QyxnQkFBZ0I7WUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7U0FDbkI7UUFDRCxJQUFJLEVBQUUsR0FBRyxHQUFHLEdBQUcsSUFBSSxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDOUQsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBMEJTLFNBQVMsQ0FBQyxPQUFtQjtRQUNuQyxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsS0FBSztZQUNOLE9BQU87UUFDWCxPQUFPO1FBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pDLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRTtZQUNYLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2xDO2FBQ0k7WUFDRCxJQUFJO2dCQUNBLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM3QztZQUNELE9BQU8sQ0FBQyxFQUFFO2dCQUNOLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDcEU7U0FFSjtJQUNMLENBQUM7SUFDRDs7Ozs7T0FLRztJQUNILElBQUksQ0FBSSxXQUFtQixFQUFFLEtBQWEsRUFBRSxHQUFHLElBQVc7UUFDdEQsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxJQUFJLEtBQUssR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDbkIsaUJBQWlCO2dCQUNqQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFBO2dCQUM3QixTQUFTO2dCQUNULGlCQUFVLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxFQUFFO29CQUN6QixJQUFJLENBQUMsRUFBRTt3QkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDO3FCQUNsRTt5QkFDSTt3QkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztxQkFDbkU7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7YUFFTDtpQkFDSTtnQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDO2FBQ2xFO1lBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDbkQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0Q7Ozs7O09BS0c7SUFDSCxhQUFhLENBQUMsTUFBZSxFQUFFLEtBQWEsRUFBRSxHQUFHLElBQVc7UUFDeEQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ25CLGlCQUFpQjtZQUNqQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQzdCLFNBQVM7WUFDVCxpQkFBVSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsRUFBRTtnQkFDekIsSUFBSSxDQUFDLEVBQUU7b0JBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUM7aUJBQzVFO3FCQUNJO29CQUNELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztpQkFDN0U7WUFDTCxDQUFDLENBQUMsQ0FBQTtTQUNMO2FBQ0k7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQztTQUM1RTtJQUNMLENBQUM7Q0FDSjtBQTdHRCxnQ0E2R0MifQ==