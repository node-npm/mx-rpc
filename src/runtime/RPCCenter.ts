import { RPCNetServer } from "../net/socket.io";
import { CompressType, gSystemSign } from "../proto";
import hashring from "hashring"
import { inflateRaw } from "zlib";
// export class DispatchModule {
//     static dispatchMap: { [name: string]: (pool: string[], reqid: string) => string } = {};
//     static dispatchMethod: string = "";
//     static registDispatchMethod(name: string, fn: (pool: string[], reqid: string) => string) {
//         this.dispatchMap[name] = fn;
//     }

//     static switchMethod(name: string) {
//         this.dispatchMethod = name;
//     }

//     static getDispatch(name?: string) {
//         if (!name) name = this.dispatchMethod;
//         if (!name) return null;
//         if (!this.dispatchMap.hasOwnProperty(name)) return null;

//         return this.dispatchMap[name];
//     }
// }

class HashringModule {
    inst: hashring;
    roles: string[];
    constructor(...ids: string[]) {
        if (ids.length > 0) {
            this.roles = [...ids];
            this.inst = new hashring(ids);
        }
        else {
            this.roles = [];
            this.inst = new hashring([]);
        }
    }

    empty() {
        return this.roles.length == 0;
    }

    add(id: string, weight?: number) {
        if (weight) {
            this.inst.add({ id: { weight: weight } });
        }
        else {
            this.inst.add(id);
        }

        this.roles.push(id);
    }

    remove(id: string) {
        this.inst.remove(id);
        let idx = this.roles.indexOf(id);
        if (idx >= 0) {
            this.roles.splice(idx, 1)
        }
    }

    dispatch(key: string) {
        if (this.roles.length == 1) return this.roles[0]
        let mq = key.split('.')[0]
        if (!mq) {
            // 如果没有指定位置那么就随机
            // let idx = Math.floor(Math.random() * this.roles.length)
            // return this.roles[idx]
            // 随机就直接走一致性hash好了
            return this.inst.get(key)
        }
        else {
            return this.inst.get(mq);
        }
    }

    foreach(callbackfn: (value: string, index: number, array: string[]) => void) {
        this.roles.forEach(callbackfn);
    }
}


// 负责转发RPC相关的消息
export class RPCCenter {
    net: RPCNetServer;
    // inside: InsideClient = gInsideClient;
    openCompress: CompressType = "normal"
    centerSign = gSystemSign;
    req2sockets: Map<string, string> = new Map;
    role2sockets: Map<string, HashringModule> = new Map;
    sockets2role: Map<string, { roles: { [role: string]: { requestCount: number, handleCount: number } }, compress: string, sign: string }> = new Map;

    constructor(srv?: RPCNetServer | number, compress?: CompressType) {
        if (typeof srv == "number") {
            this.net = new RPCNetServer(srv);
        }
        else if (srv instanceof RPCNetServer) {
            this.net = srv;
        }
        else {
            this.net = new RPCNetServer();
        }

        // 压缩类型
        this.openCompress = compress || ""

        this.net.on("close", this.onExist.bind(this));
        this.net.on("REGIST", this.onRegist.bind(this))
        // 请求
        this.net.on("RPC", this.onRPC.bind(this, ""))
        this.net.on("RPC_C", (socketid: string, reqid: string, route: string, ...args: any[]) => {
            let compressMeth = ""
            let unit = this.sockets2role.get(socketid);
            if (unit) {
                compressMeth = unit.compress;
            }

            this.onRPC(compressMeth, socketid, reqid, route, ...args)
        })

        // 应答
        this.net.on("RPCRET", this.onRPCRET.bind(this, ""))
        this.net.on("RPCRET_C", (socketid: string, reqid: string, route: string, ...args: any[]) => {
            let compressMeth = ""
            let unit = this.sockets2role.get(socketid);
            if (unit) {
                compressMeth = unit.compress;
            }

            this.onRPCRET(compressMeth, socketid, reqid, route, ...args)
        })
        // 广播
        this.net.on("RPCBC", this.onRPCBC.bind(this, ""))
        this.net.on("RPCBC_C", (socketid: string, reqid: string, toSelf: boolean, route: string, ...args: any[]) => {
            let compressMeth = ""
            let unit = this.sockets2role.get(socketid);
            if (unit) {
                compressMeth = unit.compress;
            }
            this.onRPCBC(compressMeth, socketid, reqid, toSelf, route, ...args)
        })

        // this.inside.on("RPC", this.onRPC.bind(this))
        // this.inside.on("RPCRET", this.onRPCRET.bind(this))
        // this.inside.on("RPCBC", this.onRPCBC.bind(this))
    }

    protected send(compress: boolean, socketid: string, method: string, reqid: string, route: string, ...args: any[]) {
        if (!compress) {
            return this.net.send(socketid, method, reqid, route, ...args);
        }
        // 如果数据是压缩过的
        let role = this.sockets2role.get(socketid);
        if (role?.compress != "") {
            return this.net.send(socketid, method, reqid, route, ...args)
        }

        // 那么这里需要解压后提供 向下兼容比较费效率一般不要出现
        // 这里需要解压内容，
        inflateRaw(Buffer.from(args[0]), (e, result) => {
            if (e) {
                this.net.send(socketid, method, reqid, route, ...args)
            }
            else {
                try {
                    let info = JSON.parse(result.toString())
                    // 这里成功兼容后需要处理一下信号的转换
                    this.net.send(socketid, method.replace("_C", ""), reqid, route, ...info)
                }
                catch (e) {
                    this.net.send(socketid, method, reqid, route, ...args)
                }
            }
        })
    }

    private onRegist(socketid: string, role: string, isRequest: boolean, clientSign: string, compress: string) {
        if (!isRequest) {
            let sockPool = this.role2sockets.get(role)
            if (!sockPool) {
                this.role2sockets.set(role, new HashringModule(socketid))
            }
            else {
                sockPool.add(socketid)
            }
        }
        // 回复一个事件
        this.send(false, socketid, "REGISTRET", this.centerSign, this.openCompress)
        let rolesPool = this.sockets2role.get(socketid);
        if (!rolesPool) {
            rolesPool = { roles: {}, sign: clientSign, compress: compress || "" }
            this.sockets2role.set(socketid, rolesPool);
        }

        if (!rolesPool.roles[role]) {
            rolesPool.roles[role] = {
                requestCount: 0,
                handleCount: 0,
            }
        }

        if (isRequest)
            rolesPool.roles[role].requestCount++;
        else
            rolesPool.roles[role].handleCount++;
    }

    private onExist(socketid: string) {
        let role = this.sockets2role.get(socketid)
        if (!role) {
            // 正常不存在这个情况
            return;
        }

        this.sockets2role.delete(socketid);

        for (let roleName in role.roles) {
            let rolePool = this.role2sockets.get(roleName);
            if (!rolePool) {
                // 正常不存在这个情况
                continue
            }

            rolePool.remove(socketid);
        }
    }

    /**可以通过重写分发控制器来调整 */
    protected dispatch?: (pool: string[], reqid: string, route: string) => string;

    private onRPC(compress: string, socketid: string, reqid: string, route: string, ...args: any[]) {
        // 这里route错了需要调整
        if (route == undefined || route == null || typeof route != "string") {
            console.error(...arguments);
            return;
        }

        let rouets = route.split('.');
        // 这里根据routehead 找到需要的角色
        let rolePool = this.role2sockets.get(rouets[0])
        if (!rolePool || rolePool.empty()) {
            // 表示不存在，那么直接回复404
            this.send(false, socketid, "RPCRET", reqid, route, 404)
            return;
        }

        // 收到rpc请求，这里把requestid和socketid绑定一下
        this.req2sockets.set(reqid, socketid)
        // let dispatch = DispatchModule.getDispatch() || this.dispatch;
        let chooseId = "";
        if (this.dispatch) {
            chooseId = this.dispatch(rolePool.roles, reqid, route)
        }
        else {
            chooseId = rolePool.dispatch(reqid);
        }

        this.send(compress != "", chooseId, compress == "" ? "RPC" : "RPC_C", reqid, route, ...args)
    }

    private onRPCRET(compress: string, socketid: string, reqid: string, route: string, ...args: any[]) {
        // 收到rpc请求，这里把requestid和socketid绑定一下
        let sid = this.req2sockets.get(reqid)
        if (!sid) {
            // 不存在这种情况，除非连接断开了，或者进程中断过 后续考虑是否使用消息队列或者其它高级方式
            return
        }

        this.req2sockets.delete(reqid)
        this.send(compress != "", sid, compress == "" ? "RPCRET" : "RPCRET_C", reqid, route, ...args)
    }

    private onRPCBC(compress: string, socketid: string, reqid: string, toSelf: boolean, route: string, ...args: any[]) {
        // 这里route错了需要调整
        if (route == undefined || route == null || typeof route != "string") {
            console.error(...arguments);
            return;
        }

        let rouets = route.split('.');
        // 这里根据routehead 找到需要的角色
        let rolePool = this.role2sockets.get(rouets[0])
        if (!rolePool || rolePool.empty()) return;

        // 收到rpc请求，这里把requestid和socketid绑定一下
        rolePool.foreach((sid) => {
            if (!toSelf && sid == socketid) return;
            this.send(compress != "", sid, compress == "" ? "RPCBC" : "RPCBC_C", reqid, route, ...args)
        });
        // for (let i = 0; i < rolePools.length; i++) {
        //     let sid = rolePools[i];
        //     if (!toSelf && sid == socketid) {
        //         continue;
        //     }
        //     this.send(sid, "RPCBC", reqid, route, ...args)
        // }
    }
}


// export async function test() {
//     let s = new RPCCenter()
//     let rh = new RPCHandle("name")
//     let rc = new RPCRequest("name")
//     rh.Listen("getname", function () {
//         return "chenkai"
//     })
//     let result = await rc.Call("getname", "chenkai")

//     console.log(result)
// }