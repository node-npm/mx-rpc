import { deflateRaw, inflateRaw } from "zlib";
import { RPCNetClient } from "../net/socket.io";
import { CompressType, RPCContent } from "../proto";
import { RPCNetBase } from "./RPCNetBase";
// 这里提供rpc接口服务
export class RPCRequest extends RPCNetBase {
    genRPC = 0;
    protected makeRequestID(role: string) {
        if (this.genRPC == Number.MAX_SAFE_INTEGER) {
            // 防止溢出，这里增加一个判断
            this.genRPC = 0;
        }
        let id = "R" + role + "P" + process.pid + "I" + this.genRPC++;
        return id;
    }
    mapRPCMap: Map<string, {
        reslove: (v: any) => void;
        reject(err: any): void;
    }> = new Map;
    constructor(role: string, srv?: number | RPCNetClient, compress?: CompressType) {
        super(role, true, srv, compress);
        this.on("RPCRET", this.onRPCBack.bind(this));
        this.on("RPCRET_C", (context: RPCContent) => {
            let bf = Buffer.from(context.args[0]);
            inflateRaw(bf, (e, result) => {
                if (e) {
                    this.onRPCBack(context)
                    return
                }

                try {
                    context.args = JSON.parse(result.toString());
                }
                catch (e) {
                }
                this.onRPCBack(context)
            })
        });
    }

    protected onRPCBack(context: RPCContent) {
        let code = context.args[0];
        let callP = this.mapRPCMap.get(context.requestID);
        if (!callP)
            return;
        // 移除请求
        this.mapRPCMap.delete(context.requestID);
        if (code == 0) {
            callP.reslove(context.args[1]);
        }
        else {
            try {
                callP.reject(JSON.parse(context.args[1]));
            }
            catch (e) {
                callP.reject({ code: context.args[0], errMsg: context.args[1] });
            }

        }
    }
    /**
     * 提供rpc的调用接口
     * @param dispatchKey 分发时的识别控制id
     * @param route 请求路径
     * @param args 请求参数
     */
    Call<T>(dispatchKey: string, route: string, ...args: any[]): Promise<T> {
        return new Promise((reslove, reject) => {
            let reqId = dispatchKey + '.' + this.makeRequestID(this.role);
            if (this.openCompress) {
                // 这里需要针对数据压缩处理一下
                let cm = JSON.stringify(args)
                // 调用压缩算法
                deflateRaw(cm, (e, result) => {
                    if (e) {
                        this.send("RPC", reqId, [this.role, route].join("."), ...args);
                    }
                    else {
                        this.send("RPC_C", reqId, [this.role, route].join("."), result);
                    }
                })

            }
            else {
                this.send("RPC", reqId, [this.role, route].join("."), ...args);
            }
            this.mapRPCMap.set(reqId, { reslove, reject });
        });
    }
    /**
     * 广播调用，这种就不需要统计返回结果了
     * @param toSelf 是否需要抄送自己
     * @param route
     * @param args
     */
    CallBroadcast(toSelf: boolean, route: string, ...args: any[]) {
        let reqId = this.makeRequestID(this.role);
        if (this.openCompress) {
            // 这里需要针对数据压缩处理一下
            let cm = JSON.stringify(args)
            // 调用压缩算法
            deflateRaw(cm, (e, result) => {
                if (e) {
                    this.send("RPCBC", reqId, toSelf, [this.role, route].join("."), ...args);
                }
                else {
                    this.send("RPCBC_C", reqId, toSelf, [this.role, route].join("."), result);
                }
            })
        }
        else {
            this.send("RPCBC", reqId, toSelf, [this.role, route].join("."), ...args);
        }
    }
}
