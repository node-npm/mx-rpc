"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.gSystemSign = void 0;
const crypto_1 = require("crypto");
const os = __importStar(require("os"));
// 制作一个特征码 基本这些一直的话就是一个进程中的
exports.gSystemSign = function () {
    let sys = {
        arch: os.arch(),
        cpus: os.cpus(),
        endianness: os.endianness(),
        homedir: os.homedir(),
        hostname: os.hostname(),
        networkInterfaces: os.networkInterfaces(),
        platform: os.platform(),
        release: os.release(),
        tmpdir: os.tmpdir(),
        totalmem: os.totalmem(),
        pid: process.pid,
        ppid: process.ppid,
        version: process.version
    };
    // console.log(JSON.stringify(sys))
    let sign = crypto_1.createHash("md5").update(JSON.stringify(sys)).digest("hex");
    return sign;
}();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcm90by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQW1DO0FBQ25DLHVDQUF3QjtBQVl4QiwyQkFBMkI7QUFDaEIsUUFBQSxXQUFXLEdBQUc7SUFDckIsSUFBSSxHQUFHLEdBQUc7UUFDTixJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksRUFBRTtRQUNmLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxFQUFFO1FBQ2YsVUFBVSxFQUFFLEVBQUUsQ0FBQyxVQUFVLEVBQUU7UUFDM0IsT0FBTyxFQUFFLEVBQUUsQ0FBQyxPQUFPLEVBQUU7UUFDckIsUUFBUSxFQUFFLEVBQUUsQ0FBQyxRQUFRLEVBQUU7UUFDdkIsaUJBQWlCLEVBQUUsRUFBRSxDQUFDLGlCQUFpQixFQUFFO1FBQ3pDLFFBQVEsRUFBRSxFQUFFLENBQUMsUUFBUSxFQUFFO1FBQ3ZCLE9BQU8sRUFBRSxFQUFFLENBQUMsT0FBTyxFQUFFO1FBQ3JCLE1BQU0sRUFBRSxFQUFFLENBQUMsTUFBTSxFQUFFO1FBQ25CLFFBQVEsRUFBRSxFQUFFLENBQUMsUUFBUSxFQUFFO1FBQ3ZCLEdBQUcsRUFBRSxPQUFPLENBQUMsR0FBRztRQUNoQixJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUk7UUFDbEIsT0FBTyxFQUFFLE9BQU8sQ0FBQyxPQUFPO0tBQzNCLENBQUE7SUFFRCxtQ0FBbUM7SUFDbkMsSUFBSSxJQUFJLEdBQUcsbUJBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQTtJQUV0RSxPQUFPLElBQUksQ0FBQztBQUNoQixDQUFDLEVBQUUsQ0FBQSJ9