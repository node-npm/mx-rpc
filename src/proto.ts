import { createHash } from "crypto"
import * as os from "os"

export type RPCContent = {
    requestID: string;
    type: string;
    route: string;
    args: any[];
};


export type CompressType = "" | "normal"

// 制作一个特征码 基本这些一直的话就是一个进程中的
export var gSystemSign = function () {
    let sys = {
        arch: os.arch(),
        cpus: os.cpus(),
        endianness: os.endianness(),
        homedir: os.homedir(),
        hostname: os.hostname(),
        networkInterfaces: os.networkInterfaces(),
        platform: os.platform(),
        release: os.release(),
        tmpdir: os.tmpdir(),
        totalmem: os.totalmem(),
        pid: process.pid,
        ppid: process.ppid,
        version: process.version
    }

    // console.log(JSON.stringify(sys))
    let sign = createHash("md5").update(JSON.stringify(sys)).digest("hex")

    return sign;
}()