import { EventEmitter } from "events";
// 事件模式下内部调用模式,只有内部沟通的时候才可以使用，比如所有项目都在一个进程中
export class InsideClient extends EventEmitter {
    constructor() {
        super();
        this.on("RPC", this._onData.bind(this, "RPC"));
        this.on("RPCBC", this._onData.bind(this, "RPCBC"));
        this.on("RPCRET", this._onData.bind(this, "RPCRET"));
    }
    private _onData(cmd: string, ...args: any[]) {
        this.emit("data", cmd, ...args);
    }
    send(...args: any[]) {
        this.emit(args[0], ...args.slice(1, args.length));
    }

    // 发送给center的数据
    toCenter(){
        
    }

    // 发送给client的数据
    toClient(){

    }
}
export var gInsideClient = new InsideClient();
