"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.gInsideClient = exports.InsideClient = void 0;
const events_1 = require("events");
// 事件模式下内部调用模式,只有内部沟通的时候才可以使用，比如所有项目都在一个进程中
class InsideClient extends events_1.EventEmitter {
    constructor() {
        super();
        this.on("RPC", this._onData.bind(this, "RPC"));
        this.on("RPCBC", this._onData.bind(this, "RPCBC"));
        this.on("RPCRET", this._onData.bind(this, "RPCRET"));
    }
    _onData(cmd, ...args) {
        this.emit("data", cmd, ...args);
    }
    send(...args) {
        this.emit(args[0], ...args.slice(1, args.length));
    }
    // 发送给center的数据
    toCenter() {
    }
    // 发送给client的数据
    toClient() {
    }
}
exports.InsideClient = InsideClient;
exports.gInsideClient = new InsideClient();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5zaWRlQ2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiSW5zaWRlQ2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLG1DQUFzQztBQUN0QywyQ0FBMkM7QUFDM0MsTUFBYSxZQUFhLFNBQVEscUJBQVk7SUFDMUM7UUFDSSxLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFDTyxPQUFPLENBQUMsR0FBVyxFQUFFLEdBQUcsSUFBVztRQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBQ0QsSUFBSSxDQUFDLEdBQUcsSUFBVztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELGVBQWU7SUFDZixRQUFRO0lBRVIsQ0FBQztJQUVELGVBQWU7SUFDZixRQUFRO0lBRVIsQ0FBQztDQUNKO0FBdkJELG9DQXVCQztBQUNVLFFBQUEsYUFBYSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUMifQ==