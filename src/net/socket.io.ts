// 提供socket.io的支持，服务器之间使用socket.io 连接公共云层 云层按照链路找到实际的rpc服务
import SocketIOClient from 'socket.io-client'
import SocketIO from 'socket.io'
import { EventEmitter } from "events"

var globalPort = 14001

export class RPCNetClient extends EventEmitter {
    conn: SocketIOClient.Socket;
    constructor(port?: number, host?: string, proto?: string) {
        super()
        let opt: SocketIOClient.ConnectOpts = {
            // forceNew: true,
            // autoConnect: true,
            transports: ['polling','websocket'],
            autoConnect: true
        }
        proto = proto || "http"
        host = host || "127.0.0.1"
        port = port || globalPort

        if (port == 0 || port == 80 || port == 443) {
            this.conn = SocketIOClient(`${proto}://${host}`, opt)
        }
        else {
            this.conn = SocketIOClient(`${proto}://${host}:${port}`, opt)
        }
        this.conn.on("connect", this.onConnect.bind(this, true))
        this.conn.on("connect_error", this.onConnect.bind(this, false))
        this.conn.on("disconnect", this.onClose.bind(this))
        this.conn.on("message", this.onData.bind(this))
        this.conn.on("error", this.onData.bind(this))

        // 调整最大监听数量，多个业务服用一个套接字
        this.setMaxListeners(100);
        // this.conn.connect()
    }

    on(event: "connection", listener: (succ: boolean) => void): this;
    on(event: "disconnect", listener: () => void): this;
    on(event: "data", listener: (...data: any[]) => void): this;
    on(event: string | symbol, listener: (...args: any[]) => void): this {
        super.on(event, listener);
        return this
    }
    

    connected: boolean = false;
    connectedF: boolean = false;
    onConnect(succ: boolean) {
        // console.log(arguments)
        succ ? this.connected = true : this.connectedF = true;
        this.emit("connection", succ);
    }

    onClose() {
        // console.log(arguments)
        this.emit("disconnect");
    }

    onData(...data: any[]) {
        // console.log(arguments)
        this.emit("data", ...data)
    }

    send(...args: any[]) {
        return this.conn.send(...args)
    }
}

export class RPCNetServer extends EventEmitter {
    conn: SocketIO.Server;
    constructor(port?: number) {
        super()
        let opt: SocketIO.ServerOptions = {
            transports: ['websocket', "polling"],
            allowUpgrades: true
        }
        this.conn = SocketIO.listen(port || globalPort, opt)
        this.conn.on("connection", this.onConnection.bind(this))
    }

    on(event: "connection" | "disconnect" | string, listener: (socketid: string) => void): this;
    on(event: "data" | string, listener: (socketid: string, ...data: any[]) => void): this;
    on(event: string | symbol, listener: (socketid: string, ...args: any[]) => void): this {
        super.on(event, listener);
        return this
    }

    onConnection(socket: SocketIO.Socket) {
        socket.on("message", this.onSocketData.bind(this, socket));
        socket.on("disconnect", this.onSocketClose.bind(this, socket));
        // console.log(arguments)
        this.emit("connection", socket.id)
    }

    onSocketData(socket: SocketIO.Socket, ...data: any[]) {
        // console.log(arguments)
        // 这里需要解析一下内容了 这里我们把传输过来的第一个参数作为 event 后续的作为参数
        let event = data[0];
        // console.log("onSocketData", JSON.stringify(data.slice(0, 3)))
        if (this.listenerCount(event) == 0) {
            this.emit("data", socket.id, ...data)
        }
        else {
            this.emit(event, socket.id, ...data.slice(1, data.length))
        }
    }

    onSocketClose(socket: SocketIO.Socket) {
        // console.log(arguments)
        this.emit("close", socket.id)
    }

    send(socketid: string, ...args: any[]) {
        let socket = this.conn.sockets.sockets[socketid];
        if (socket) {
            socket.send(...args);
        }
    }
}

// export function test() {
//     let s = new Server()
//     let c = new Client()

//     s.on("connection", function (ns) {
//         console.log(ns)
//     })

//     c.on("connection", function () {
//         console.log()
//     })
// }