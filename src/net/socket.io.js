"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RPCNetServer = exports.RPCNetClient = void 0;
// 提供socket.io的支持，服务器之间使用socket.io 连接公共云层 云层按照链路找到实际的rpc服务
const socket_io_client_1 = __importDefault(require("socket.io-client"));
const socket_io_1 = __importDefault(require("socket.io"));
const events_1 = require("events");
var globalPort = 14001;
class RPCNetClient extends events_1.EventEmitter {
    constructor(port, host, proto) {
        super();
        this.connected = false;
        this.connectedF = false;
        let opt = {
            // forceNew: true,
            // autoConnect: true,
            transports: ['polling', 'websocket'],
            autoConnect: true
        };
        proto = proto || "http";
        host = host || "127.0.0.1";
        port = port || globalPort;
        if (port == 0 || port == 80 || port == 443) {
            this.conn = socket_io_client_1.default(`${proto}://${host}`, opt);
        }
        else {
            this.conn = socket_io_client_1.default(`${proto}://${host}:${port}`, opt);
        }
        this.conn.on("connect", this.onConnect.bind(this, true));
        this.conn.on("connect_error", this.onConnect.bind(this, false));
        this.conn.on("disconnect", this.onClose.bind(this));
        this.conn.on("message", this.onData.bind(this));
        this.conn.on("error", this.onData.bind(this));
        // 调整最大监听数量，多个业务服用一个套接字
        this.setMaxListeners(100);
        // this.conn.connect()
    }
    on(event, listener) {
        super.on(event, listener);
        return this;
    }
    onConnect(succ) {
        // console.log(arguments)
        succ ? this.connected = true : this.connectedF = true;
        this.emit("connection", succ);
    }
    onClose() {
        // console.log(arguments)
        this.emit("disconnect");
    }
    onData(...data) {
        // console.log(arguments)
        this.emit("data", ...data);
    }
    send(...args) {
        return this.conn.send(...args);
    }
}
exports.RPCNetClient = RPCNetClient;
class RPCNetServer extends events_1.EventEmitter {
    constructor(port) {
        super();
        let opt = {
            transports: ['websocket', "polling"],
            allowUpgrades: true
        };
        this.conn = socket_io_1.default.listen(port || globalPort, opt);
        this.conn.on("connection", this.onConnection.bind(this));
    }
    on(event, listener) {
        super.on(event, listener);
        return this;
    }
    onConnection(socket) {
        socket.on("message", this.onSocketData.bind(this, socket));
        socket.on("disconnect", this.onSocketClose.bind(this, socket));
        // console.log(arguments)
        this.emit("connection", socket.id);
    }
    onSocketData(socket, ...data) {
        // console.log(arguments)
        // 这里需要解析一下内容了 这里我们把传输过来的第一个参数作为 event 后续的作为参数
        let event = data[0];
        // console.log("onSocketData", JSON.stringify(data.slice(0, 3)))
        if (this.listenerCount(event) == 0) {
            this.emit("data", socket.id, ...data);
        }
        else {
            this.emit(event, socket.id, ...data.slice(1, data.length));
        }
    }
    onSocketClose(socket) {
        // console.log(arguments)
        this.emit("close", socket.id);
    }
    send(socketid, ...args) {
        let socket = this.conn.sockets.sockets[socketid];
        if (socket) {
            socket.send(...args);
        }
    }
}
exports.RPCNetServer = RPCNetServer;
// export function test() {
//     let s = new Server()
//     let c = new Client()
//     s.on("connection", function (ns) {
//         console.log(ns)
//     })
//     c.on("connection", function () {
//         console.log()
//     })
// }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ja2V0LmlvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic29ja2V0LmlvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLDBEQUEwRDtBQUMxRCx3RUFBNkM7QUFDN0MsMERBQWdDO0FBQ2hDLG1DQUFxQztBQUVyQyxJQUFJLFVBQVUsR0FBRyxLQUFLLENBQUE7QUFFdEIsTUFBYSxZQUFhLFNBQVEscUJBQVk7SUFFMUMsWUFBWSxJQUFhLEVBQUUsSUFBYSxFQUFFLEtBQWM7UUFDcEQsS0FBSyxFQUFFLENBQUE7UUFxQ1gsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixlQUFVLEdBQVksS0FBSyxDQUFDO1FBckN4QixJQUFJLEdBQUcsR0FBK0I7WUFDbEMsa0JBQWtCO1lBQ2xCLHFCQUFxQjtZQUNyQixVQUFVLEVBQUUsQ0FBQyxTQUFTLEVBQUMsV0FBVyxDQUFDO1lBQ25DLFdBQVcsRUFBRSxJQUFJO1NBQ3BCLENBQUE7UUFDRCxLQUFLLEdBQUcsS0FBSyxJQUFJLE1BQU0sQ0FBQTtRQUN2QixJQUFJLEdBQUcsSUFBSSxJQUFJLFdBQVcsQ0FBQTtRQUMxQixJQUFJLEdBQUcsSUFBSSxJQUFJLFVBQVUsQ0FBQTtRQUV6QixJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUUsSUFBSSxJQUFJLElBQUksR0FBRyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxJQUFJLEdBQUcsMEJBQWMsQ0FBQyxHQUFHLEtBQUssTUFBTSxJQUFJLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQTtTQUN4RDthQUNJO1lBQ0QsSUFBSSxDQUFDLElBQUksR0FBRywwQkFBYyxDQUFDLEdBQUcsS0FBSyxNQUFNLElBQUksSUFBSSxJQUFJLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQTtTQUNoRTtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUN4RCxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUE7UUFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7UUFDL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7UUFFN0MsdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDMUIsc0JBQXNCO0lBQzFCLENBQUM7SUFLRCxFQUFFLENBQUMsS0FBc0IsRUFBRSxRQUFrQztRQUN6RCxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMxQixPQUFPLElBQUksQ0FBQTtJQUNmLENBQUM7SUFLRCxTQUFTLENBQUMsSUFBYTtRQUNuQix5QkFBeUI7UUFDekIsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELE9BQU87UUFDSCx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsTUFBTSxDQUFDLEdBQUcsSUFBVztRQUNqQix5QkFBeUI7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQTtJQUM5QixDQUFDO0lBRUQsSUFBSSxDQUFDLEdBQUcsSUFBVztRQUNmLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQTtJQUNsQyxDQUFDO0NBQ0o7QUE3REQsb0NBNkRDO0FBRUQsTUFBYSxZQUFhLFNBQVEscUJBQVk7SUFFMUMsWUFBWSxJQUFhO1FBQ3JCLEtBQUssRUFBRSxDQUFBO1FBQ1AsSUFBSSxHQUFHLEdBQTJCO1lBQzlCLFVBQVUsRUFBRSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUM7WUFDcEMsYUFBYSxFQUFFLElBQUk7U0FDdEIsQ0FBQTtRQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsbUJBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQTtRQUNwRCxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtJQUM1RCxDQUFDO0lBSUQsRUFBRSxDQUFDLEtBQXNCLEVBQUUsUUFBb0Q7UUFDM0UsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUE7SUFDZixDQUFDO0lBRUQsWUFBWSxDQUFDLE1BQXVCO1FBQ2hDLE1BQU0sQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQzNELE1BQU0sQ0FBQyxFQUFFLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQy9ELHlCQUF5QjtRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUE7SUFDdEMsQ0FBQztJQUVELFlBQVksQ0FBQyxNQUF1QixFQUFFLEdBQUcsSUFBVztRQUNoRCx5QkFBeUI7UUFDekIsOENBQThDO1FBQzlDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwQixnRUFBZ0U7UUFDaEUsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsRUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUE7U0FDeEM7YUFDSTtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxFQUFFLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQTtTQUM3RDtJQUNMLENBQUM7SUFFRCxhQUFhLENBQUMsTUFBdUI7UUFDakMseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUNqQyxDQUFDO0lBRUQsSUFBSSxDQUFDLFFBQWdCLEVBQUUsR0FBRyxJQUFXO1FBQ2pDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqRCxJQUFJLE1BQU0sRUFBRTtZQUNSLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztTQUN4QjtJQUNMLENBQUM7Q0FDSjtBQWxERCxvQ0FrREM7QUFFRCwyQkFBMkI7QUFDM0IsMkJBQTJCO0FBQzNCLDJCQUEyQjtBQUUzQix5Q0FBeUM7QUFDekMsMEJBQTBCO0FBQzFCLFNBQVM7QUFFVCx1Q0FBdUM7QUFDdkMsd0JBQXdCO0FBQ3hCLFNBQVM7QUFDVCxJQUFJIn0=