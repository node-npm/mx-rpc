import { sep } from "path";
import { deflateRaw, inflateRaw } from "zlib";
import { RPCNetClient } from "./net/socket.io";
import { RPCContent } from "./proto";
import { RPCNetBase } from "./runtime/RPCNetBase";


function parseErrorLine(err: Error) {
    if (!err || !(err instanceof Error) || !err.stack) {
        return '[-]'
    }

    const stack = err.stack;
    const stackArr = stack.split('\n');
    let callerLogIndex = (stackArr.length > 0) ? 1 : 0;
    // for (let i = 0; i < stackArr.length; i++) {
    //     if (stackArr[i].indexOf('makeError') > 0 && i + 1 < stackArr.length) {
    //         callerLogIndex = i + 1;
    //         break;
    //     }
    // }

    if (callerLogIndex !== 0) {
        const callerStackLine = stackArr[callerLogIndex] as string;
        let list = callerStackLine.split(/\(|\)/)
        if (list[1] && list[1].length > 0) {
            return `[${list[1].replace(process.cwd(), '').replace(/\\/g, "/").replace('/', '')}]`
        }
        else {
            return `[${callerStackLine.substring(callerStackLine.lastIndexOf(sep) + 1, callerStackLine.lastIndexOf(':'))}]`;
        }
    } else {
        return '[-]';
    }
}


interface IFRPCResponse {
    (...args: any[]): Promise<any> | any
}

export type SeParamConfigType = "bigint" | "boolean" | "function" | "number" | "object" | "string" | "symbol"
interface ifParamCheckConfig { [name: string]: { type: SeParamConfigType, idx: number, required: boolean, change: boolean } }
interface TypedPropertyDescriptorEx<T> extends TypedPropertyDescriptor<T> {
    params?: ifParamCheckConfig
}
class HandleUnit {
    /**
    * 注册一个初始化模块，返回一个init方法
    * @param md 
    */
    class(md: NodeModule): ClassDecorator {
        return (target) => {
            md.exports.name = this.role + "Serve"
            md.exports.init = this._init.bind(this)
        }
    }

    /**
     * 注册模块初始化时调用的装饰器
     */
    init(): MethodDecorator {
        return (target, name, sepctor) => {
            if (!sepctor.value) return;
            this.initPools.push((sepctor.value as any).bind(target))
        }
    }

    /**
     * 注册一个路由响应
     * @param event 监听事件名字 默认使用方法名字
     */
    route(event?: string): MethodDecorator {
        return (target, name, sepctor) => {
            if (!event && typeof name == "string") event = name;
            if (sepctor.value && event) this.registRoute(event, target, sepctor)
        }
    }

    merge(unit: HandleUnit) {
        if (unit.initPools.length > 0) {
            this.initPools.push(...unit.initPools)
        }

        unit.mapRPC.forEach((value, key) => {
            this.mapRPC.set(key, value)
        })
    }

    methodPrepare(descriptor: TypedPropertyDescriptorEx<any>) {
        if (!descriptor.params) descriptor.params = {};
    }

    /**
   * 检查路由参数 如果调用了那么就给这个函数增加一个参数检查的标识，否则就不处理
   */
    params(descriptor: TypedPropertyDescriptorEx<any>, pName: string, pType: SeParamConfigType, required: boolean, change: boolean) {
        this.methodPrepare(descriptor)
        if (descriptor.params) {
            descriptor.params[pName] = { type: pType, idx: -1, required: required, change: change }
        }
    }

    // 每个接口服务都有一个角色，决定rpc的角色
    private mapRPC: Map<string, { target: any; value: IFRPCResponse, params: ifParamCheckConfig }> = new Map
    private net!: RPCNetBase;

    private initPools: (() => Promise<any>)[] = []

    private role: string = "";
    symbol: symbol;
    constructor(symbol?: symbol, role?: string) {
        if (role) this.role = role;
        this.symbol = symbol || Symbol()
    }

    /**
     * 设置一下rpc的模块名字
     * @param role 
     */
    setRole(role: string) {
        this.role = role;
    }

    async _init(srv?: number | RPCNetClient) {
        if (!this.net) {
            this.net = new RPCNetBase(this.role, false, srv, "normal")
            this.net.on("RPC", this.rpcRequest.bind(this))
            this.net.on("RPC_C", (context: RPCContent) => {
                let bf = context.args[0] as any;
                inflateRaw(bf, (e, result) => {
                    if (e) {
                        this.rpcRequest(context)
                        return
                    }

                    try {
                        context.args = JSON.parse(result.toString());
                    }
                    catch (e) {
                    }
                    this.rpcRequest(context)
                })
            })

            this.net.on("RPCBC", this.rpcBCRequest.bind(this))
            this.net.on("RPCBC_C", (context: RPCContent) => {
                let bf = Buffer.from(context.args);
                inflateRaw(bf, (e, result) => {
                    if (e) {
                        this.rpcBCRequest(context)
                        return
                    }

                    try {
                        context.args = JSON.parse(result.toString());
                    }
                    catch (e) {
                    }
                    this.rpcBCRequest(context)
                })
            })
        }

        await this.net.init()

        for (; this.initPools.length > 0;) {
            await this.initPools[0]()
            this.initPools.splice(0, 1)
        }

        return true;
    }

    // 提供rpc的监听接口
    registRoute(route: string, target: any, sepctor: TypedPropertyDescriptorEx<any>) {
        // 这里需要做一个简单处理，把参数和位置对应好
        function findArgs(func: string) {
            let ss = func.toString().split('\n')[0].split('(')[1].split(')')[0].split(',');
            if (ss.length == 1 && ss[0].trim() == "") {
                return []
            }

            for (let i = 0; i < ss.length; i++) {
                ss[i] = ss[i].trim()
            }

            return ss
        }

        let args = findArgs(sepctor.value)
        let params = sepctor.params || {};
        for (let key in params) {
            params[key].idx = args.indexOf(key);
            if (params[key].idx < 0) continue;
        }
        this.mapRPC.set(route, { target: target, value: sepctor.value, params: params })
    }

    registInit(func: IFRPCResponse) {
        this.initPools.push(func)
    }

    paramsCheck(checkMap: ifParamCheckConfig, params: any[]) {
        if (checkMap == undefined) return;

        for (let name in checkMap) {
            let cfg = checkMap[name];
            if (cfg.idx < 0) continue
            let key = cfg.idx;
            if (cfg.required && !params[cfg.idx]) {
                // 这里没找到 抛出一个异常
                throw Error(`param [${name} required and type limit ${cfg.type}]`)
            }
            else if (!params[key]) {
                continue;
            }

            if (cfg.type == typeof params[key]) {
                continue
            }

            // 这里类型不同
            if (!cfg.change) {
                throw Error(`param [${key} type limit ${cfg.type}]`)
            }
            else {
                switch (cfg.type) {
                    case "bigint": params[key] = BigInt(params[key]); break;
                    case "boolean": params[key] = Boolean(params[key]); break;
                    case "function": throw Error(`param [${key} type limit ${cfg.type}]`);
                    case "number": params[key] = Number(params[key]);
                        // 这里增加一个小优化 ，如果是整数那么直接转换成整数
                        if (params[key] = parseInt(params[key])) {
                            params[key] = parseInt(params[key])
                        }
                        break;
                    case "object": params[key] = JSON.parse(params[key].toString()); break;
                    case "string": params[key] = String(params[key]); break;
                    case "symbol": params[key] = Symbol(params[key]); break;
                    default: break;
                }
            }
        }
    }

    protected async rpcRequest(context: RPCContent) {
        // console.log(context.route)
        let handle = this.mapRPC.get(context.route);
        if (!handle) {
            // 找不到，回复类似404的异常错误
            this.response(context, 404)
            return;
        }

        try {
            this.paramsCheck(handle.params, context.args)
            let v: any = handle.value.apply(handle.target, context.args);
            if (v instanceof Promise) {
                v = await v
            }

            if (v && v.code != undefined && v.code != 0) {
                this.response(context, v.code, v.errMsg)
            }
            else {
                this.response(context, 0, v)
            }
        }
        catch (e) {
            if (!e) {
                e = {
                    code: -1,
                    errMsg: "error bug no message"
                }
            }
            else if (e instanceof Error) {
                e = {
                    code: -1,
                    errMsg: e.message + "   " + parseErrorLine(e),
                }
            }

            let eMsg = JSON.stringify(e);

            this.response(context, e.code, eMsg)
        }
    }

    protected async rpcBCRequest(context: RPCContent) {
        let handle = this.mapRPC.get(context.route);
        if (!handle) return;

        try {
            this.paramsCheck(handle.params, context.args)
            let v: any = handle.value.apply(handle.target, context.args);
            if (v instanceof Promise) {
                v = await v
            }
        }
        catch (e) {
            if (!e) {
                e = {
                    code: -1,
                    errMsg: "error bug no message"
                }
            }
            else if (e instanceof Error) {
                e = {
                    code: -1,
                    errMsg: e.message + "   " + parseErrorLine(e),
                }
            }
            console.log("rpc bc notice", e)
        }
    }

    protected response(context: RPCContent, code: number, msg?: any) {
        if (code == 404) {
            console.log(404, context)
        }

        // 这里压缩返回
        if (this.net.openCompress) {
            deflateRaw(JSON.stringify([code, msg]), (e, result) => {
                if (e) {
                    this.net.send("RPCRET", context.requestID, this.role + "." + context.route, code, msg)
                }
                else {
                    this.net.send("RPCRET_C", context.requestID, this.role + "." + context.route, result)
                }
            })
        }
        else {
            this.net.send("RPCRET", context.requestID, this.role + "." + context.route, code, msg)
        }
    }
}

// 这里提供rpc注册服务
export class RPCHandle {

    private static mapRole: Map<string, HandleUnit> = new Map;
    private static currRole: HandleUnit | undefined;
    //获取模块
    static getRole(role: string) {
        if (!this.mapRole.has(role)) {
            this.mapRole.set(role, new HandleUnit(undefined, role))
        }

        return this.mapRole.get(role) as HandleUnit
    }

    //模块初始化
    static doInit(role: string, srv?: number | RPCNetClient) {
        return this.getRole(role)._init(srv)
    }

    private static _chechMethod(target: any) {
        if (!this.currRole) this.currRole = new HandleUnit()
        if (!target.___symbol) target.___symbol = this.currRole.symbol;
        if (target.___symbol != this.currRole.symbol) {
            // 表示注册的不是当前class的模块了，需要替换
            this.currRole = new HandleUnit
        }
    }

    /**
     * 注册一个初始化模块，返回一个init方法
     * @param md 
     */
    static class(role: string, md: NodeModule): ClassDecorator {
        if (this.currRole) {
            this.currRole.setRole(role)

            if (this.mapRole.has(role)) {
                // 需要合并一下模块
                let mp = this.mapRole.get(role) as HandleUnit
                mp.merge(this.currRole)
                this.currRole = undefined;
            }
            else {
                this.mapRole.set(role, this.currRole)
            }
            this.currRole = undefined

            return (target) => {
                md.exports.name = role + "Serve"
                md.exports.init = this.doInit.bind(this, role)
            }
        }
        else {
            return this.getRole(role).class(md);
        }
    }

    /**
     * 注册模块初始化时调用的装饰器
     */
    static init(): MethodDecorator {
        return (target, name, sepctor) => {
            if (!sepctor.value) return;
            this._chechMethod(target)
            this.currRole && this.currRole.registInit((sepctor.value as any).bind(target))
        }
    }

    /**
     * 注册一个路由响应
     * @param event 监听事件名字 默认使用方法名字
     */
    static route(event?: string): MethodDecorator {
        return (target, name, sepctor) => {
            if (!event && typeof name == "string") event = name;
            if (!sepctor.value || !event) return
            this._chechMethod(target)
            this.currRole && this.currRole.registRoute(event, target, sepctor)
        }
    }

    /**
    * 检查参数
    * @param name 参数名字
    * @param sType 参数类型
    * @param [change] 是否强制转换类型 
    */
    static paramRequired(pName: string, sType: SeParamConfigType, change: boolean): MethodDecorator {
        return (target, name, sepctor) => {
            this._chechMethod(target)
            if (this.currRole) {
                this.currRole.params(sepctor, pName, sType, true, change)
            }
        }
    }
}
