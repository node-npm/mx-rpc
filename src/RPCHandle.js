"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RPCHandle = void 0;
const path_1 = require("path");
const zlib_1 = require("zlib");
const RPCNetBase_1 = require("./runtime/RPCNetBase");
function parseErrorLine(err) {
    if (!err || !(err instanceof Error) || !err.stack) {
        return '[-]';
    }
    const stack = err.stack;
    const stackArr = stack.split('\n');
    let callerLogIndex = (stackArr.length > 0) ? 1 : 0;
    // for (let i = 0; i < stackArr.length; i++) {
    //     if (stackArr[i].indexOf('makeError') > 0 && i + 1 < stackArr.length) {
    //         callerLogIndex = i + 1;
    //         break;
    //     }
    // }
    if (callerLogIndex !== 0) {
        const callerStackLine = stackArr[callerLogIndex];
        let list = callerStackLine.split(/\(|\)/);
        if (list[1] && list[1].length > 0) {
            return `[${list[1].replace(process.cwd(), '').replace(/\\/g, "/").replace('/', '')}]`;
        }
        else {
            return `[${callerStackLine.substring(callerStackLine.lastIndexOf(path_1.sep) + 1, callerStackLine.lastIndexOf(':'))}]`;
        }
    }
    else {
        return '[-]';
    }
}
class HandleUnit {
    constructor(symbol, role) {
        // 每个接口服务都有一个角色，决定rpc的角色
        this.mapRPC = new Map;
        this.initPools = [];
        this.role = "";
        if (role)
            this.role = role;
        this.symbol = symbol || Symbol();
    }
    /**
    * 注册一个初始化模块，返回一个init方法
    * @param md
    */
    class(md) {
        return (target) => {
            md.exports.name = this.role + "Serve";
            md.exports.init = this._init.bind(this);
        };
    }
    /**
     * 注册模块初始化时调用的装饰器
     */
    init() {
        return (target, name, sepctor) => {
            if (!sepctor.value)
                return;
            this.initPools.push(sepctor.value.bind(target));
        };
    }
    /**
     * 注册一个路由响应
     * @param event 监听事件名字 默认使用方法名字
     */
    route(event) {
        return (target, name, sepctor) => {
            if (!event && typeof name == "string")
                event = name;
            if (sepctor.value && event)
                this.registRoute(event, target, sepctor);
        };
    }
    merge(unit) {
        if (unit.initPools.length > 0) {
            this.initPools.push(...unit.initPools);
        }
        unit.mapRPC.forEach((value, key) => {
            this.mapRPC.set(key, value);
        });
    }
    methodPrepare(descriptor) {
        if (!descriptor.params)
            descriptor.params = {};
    }
    /**
   * 检查路由参数 如果调用了那么就给这个函数增加一个参数检查的标识，否则就不处理
   */
    params(descriptor, pName, pType, required, change) {
        this.methodPrepare(descriptor);
        if (descriptor.params) {
            descriptor.params[pName] = { type: pType, idx: -1, required: required, change: change };
        }
    }
    /**
     * 设置一下rpc的模块名字
     * @param role
     */
    setRole(role) {
        this.role = role;
    }
    _init(srv) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.net) {
                this.net = new RPCNetBase_1.RPCNetBase(this.role, false, srv, "normal");
                this.net.on("RPC", this.rpcRequest.bind(this));
                this.net.on("RPC_C", (context) => {
                    let bf = context.args[0];
                    zlib_1.inflateRaw(bf, (e, result) => {
                        if (e) {
                            this.rpcRequest(context);
                            return;
                        }
                        try {
                            context.args = JSON.parse(result.toString());
                        }
                        catch (e) {
                        }
                        this.rpcRequest(context);
                    });
                });
                this.net.on("RPCBC", this.rpcBCRequest.bind(this));
                this.net.on("RPCBC_C", (context) => {
                    let bf = Buffer.from(context.args);
                    zlib_1.inflateRaw(bf, (e, result) => {
                        if (e) {
                            this.rpcBCRequest(context);
                            return;
                        }
                        try {
                            context.args = JSON.parse(result.toString());
                        }
                        catch (e) {
                        }
                        this.rpcBCRequest(context);
                    });
                });
            }
            yield this.net.init();
            for (; this.initPools.length > 0;) {
                yield this.initPools[0]();
                this.initPools.splice(0, 1);
            }
            return true;
        });
    }
    // 提供rpc的监听接口
    registRoute(route, target, sepctor) {
        // 这里需要做一个简单处理，把参数和位置对应好
        function findArgs(func) {
            let ss = func.toString().split('\n')[0].split('(')[1].split(')')[0].split(',');
            if (ss.length == 1 && ss[0].trim() == "") {
                return [];
            }
            for (let i = 0; i < ss.length; i++) {
                ss[i] = ss[i].trim();
            }
            return ss;
        }
        let args = findArgs(sepctor.value);
        let params = sepctor.params || {};
        for (let key in params) {
            params[key].idx = args.indexOf(key);
            if (params[key].idx < 0)
                continue;
        }
        this.mapRPC.set(route, { target: target, value: sepctor.value, params: params });
    }
    registInit(func) {
        this.initPools.push(func);
    }
    paramsCheck(checkMap, params) {
        if (checkMap == undefined)
            return;
        for (let name in checkMap) {
            let cfg = checkMap[name];
            if (cfg.idx < 0)
                continue;
            let key = cfg.idx;
            if (cfg.required && !params[cfg.idx]) {
                // 这里没找到 抛出一个异常
                throw Error(`param [${name} required and type limit ${cfg.type}]`);
            }
            else if (!params[key]) {
                continue;
            }
            if (cfg.type == typeof params[key]) {
                continue;
            }
            // 这里类型不同
            if (!cfg.change) {
                throw Error(`param [${key} type limit ${cfg.type}]`);
            }
            else {
                switch (cfg.type) {
                    case "bigint":
                        params[key] = BigInt(params[key]);
                        break;
                    case "boolean":
                        params[key] = Boolean(params[key]);
                        break;
                    case "function": throw Error(`param [${key} type limit ${cfg.type}]`);
                    case "number":
                        params[key] = Number(params[key]);
                        // 这里增加一个小优化 ，如果是整数那么直接转换成整数
                        if (params[key] = parseInt(params[key])) {
                            params[key] = parseInt(params[key]);
                        }
                        break;
                    case "object":
                        params[key] = JSON.parse(params[key].toString());
                        break;
                    case "string":
                        params[key] = String(params[key]);
                        break;
                    case "symbol":
                        params[key] = Symbol(params[key]);
                        break;
                    default: break;
                }
            }
        }
    }
    rpcRequest(context) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log(context.route)
            let handle = this.mapRPC.get(context.route);
            if (!handle) {
                // 找不到，回复类似404的异常错误
                this.response(context, 404);
                return;
            }
            try {
                this.paramsCheck(handle.params, context.args);
                let v = handle.value.apply(handle.target, context.args);
                if (v instanceof Promise) {
                    v = yield v;
                }
                if (v && v.code != undefined && v.code != 0) {
                    this.response(context, v.code, v.errMsg);
                }
                else {
                    this.response(context, 0, v);
                }
            }
            catch (e) {
                if (!e) {
                    e = {
                        code: -1,
                        errMsg: "error bug no message"
                    };
                }
                else if (e instanceof Error) {
                    e = {
                        code: -1,
                        errMsg: e.message + "   " + parseErrorLine(e),
                    };
                }
                let eMsg = JSON.stringify(e);
                this.response(context, e.code, eMsg);
            }
        });
    }
    rpcBCRequest(context) {
        return __awaiter(this, void 0, void 0, function* () {
            let handle = this.mapRPC.get(context.route);
            if (!handle)
                return;
            try {
                this.paramsCheck(handle.params, context.args);
                let v = handle.value.apply(handle.target, context.args);
                if (v instanceof Promise) {
                    v = yield v;
                }
            }
            catch (e) {
                if (!e) {
                    e = {
                        code: -1,
                        errMsg: "error bug no message"
                    };
                }
                else if (e instanceof Error) {
                    e = {
                        code: -1,
                        errMsg: e.message + "   " + parseErrorLine(e),
                    };
                }
                console.log("rpc bc notice", e);
            }
        });
    }
    response(context, code, msg) {
        if (code == 404) {
            console.log(404, context);
        }
        // 这里压缩返回
        if (this.net.openCompress) {
            zlib_1.deflateRaw(JSON.stringify([code, msg]), (e, result) => {
                if (e) {
                    this.net.send("RPCRET", context.requestID, this.role + "." + context.route, code, msg);
                }
                else {
                    this.net.send("RPCRET_C", context.requestID, this.role + "." + context.route, result);
                }
            });
        }
        else {
            this.net.send("RPCRET", context.requestID, this.role + "." + context.route, code, msg);
        }
    }
}
// 这里提供rpc注册服务
class RPCHandle {
    //获取模块
    static getRole(role) {
        if (!this.mapRole.has(role)) {
            this.mapRole.set(role, new HandleUnit(undefined, role));
        }
        return this.mapRole.get(role);
    }
    //模块初始化
    static doInit(role, srv) {
        return this.getRole(role)._init(srv);
    }
    static _chechMethod(target) {
        if (!this.currRole)
            this.currRole = new HandleUnit();
        if (!target.___symbol)
            target.___symbol = this.currRole.symbol;
        if (target.___symbol != this.currRole.symbol) {
            // 表示注册的不是当前class的模块了，需要替换
            this.currRole = new HandleUnit;
        }
    }
    /**
     * 注册一个初始化模块，返回一个init方法
     * @param md
     */
    static class(role, md) {
        if (this.currRole) {
            this.currRole.setRole(role);
            if (this.mapRole.has(role)) {
                // 需要合并一下模块
                let mp = this.mapRole.get(role);
                mp.merge(this.currRole);
                this.currRole = undefined;
            }
            else {
                this.mapRole.set(role, this.currRole);
            }
            this.currRole = undefined;
            return (target) => {
                md.exports.name = role + "Serve";
                md.exports.init = this.doInit.bind(this, role);
            };
        }
        else {
            return this.getRole(role).class(md);
        }
    }
    /**
     * 注册模块初始化时调用的装饰器
     */
    static init() {
        return (target, name, sepctor) => {
            if (!sepctor.value)
                return;
            this._chechMethod(target);
            this.currRole && this.currRole.registInit(sepctor.value.bind(target));
        };
    }
    /**
     * 注册一个路由响应
     * @param event 监听事件名字 默认使用方法名字
     */
    static route(event) {
        return (target, name, sepctor) => {
            if (!event && typeof name == "string")
                event = name;
            if (!sepctor.value || !event)
                return;
            this._chechMethod(target);
            this.currRole && this.currRole.registRoute(event, target, sepctor);
        };
    }
    /**
    * 检查参数
    * @param name 参数名字
    * @param sType 参数类型
    * @param [change] 是否强制转换类型
    */
    static paramRequired(pName, sType, change) {
        return (target, name, sepctor) => {
            this._chechMethod(target);
            if (this.currRole) {
                this.currRole.params(sepctor, pName, sType, true, change);
            }
        };
    }
}
exports.RPCHandle = RPCHandle;
RPCHandle.mapRole = new Map;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUlBDSGFuZGxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiUlBDSGFuZGxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLCtCQUEyQjtBQUMzQiwrQkFBOEM7QUFHOUMscURBQWtEO0FBR2xELFNBQVMsY0FBYyxDQUFDLEdBQVU7SUFDOUIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxZQUFZLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRTtRQUMvQyxPQUFPLEtBQUssQ0FBQTtLQUNmO0lBRUQsTUFBTSxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztJQUN4QixNQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ25DLElBQUksY0FBYyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbkQsOENBQThDO0lBQzlDLDZFQUE2RTtJQUM3RSxrQ0FBa0M7SUFDbEMsaUJBQWlCO0lBQ2pCLFFBQVE7SUFDUixJQUFJO0lBRUosSUFBSSxjQUFjLEtBQUssQ0FBQyxFQUFFO1FBQ3RCLE1BQU0sZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQVcsQ0FBQztRQUMzRCxJQUFJLElBQUksR0FBRyxlQUFlLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQ3pDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQy9CLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQTtTQUN4RjthQUNJO1lBQ0QsT0FBTyxJQUFJLGVBQWUsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsZUFBZSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUM7U0FDbkg7S0FDSjtTQUFNO1FBQ0gsT0FBTyxLQUFLLENBQUM7S0FDaEI7QUFDTCxDQUFDO0FBWUQsTUFBTSxVQUFVO0lBaUVaLFlBQVksTUFBZSxFQUFFLElBQWE7UUFSMUMsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBbUYsSUFBSSxHQUFHLENBQUE7UUFHaEcsY0FBUyxHQUEyQixFQUFFLENBQUE7UUFFdEMsU0FBSSxHQUFXLEVBQUUsQ0FBQztRQUd0QixJQUFJLElBQUk7WUFBRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sSUFBSSxNQUFNLEVBQUUsQ0FBQTtJQUNwQyxDQUFDO0lBbkVEOzs7TUFHRTtJQUNGLEtBQUssQ0FBQyxFQUFjO1FBQ2hCLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUNkLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFBO1lBQ3JDLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzNDLENBQUMsQ0FBQTtJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNILElBQUk7UUFDQSxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQUUsT0FBTztZQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBRSxPQUFPLENBQUMsS0FBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFBO1FBQzVELENBQUMsQ0FBQTtJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSCxLQUFLLENBQUMsS0FBYztRQUNoQixPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRTtZQUM3QixJQUFJLENBQUMsS0FBSyxJQUFJLE9BQU8sSUFBSSxJQUFJLFFBQVE7Z0JBQUUsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNwRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksS0FBSztnQkFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFDeEUsQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQUVELEtBQUssQ0FBQyxJQUFnQjtRQUNsQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtTQUN6QztRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUMvQixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRCxhQUFhLENBQUMsVUFBMEM7UUFDcEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNO1lBQUUsVUFBVSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7SUFDbkQsQ0FBQztJQUVEOztLQUVDO0lBQ0QsTUFBTSxDQUFDLFVBQTBDLEVBQUUsS0FBYSxFQUFFLEtBQXdCLEVBQUUsUUFBaUIsRUFBRSxNQUFlO1FBQzFILElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDOUIsSUFBSSxVQUFVLENBQUMsTUFBTSxFQUFFO1lBQ25CLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQTtTQUMxRjtJQUNMLENBQUM7SUFlRDs7O09BR0c7SUFDSCxPQUFPLENBQUMsSUFBWTtRQUNoQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRUssS0FBSyxDQUFDLEdBQTJCOztZQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRTtnQkFDWCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksdUJBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUE7Z0JBQzFELElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO2dCQUM5QyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxPQUFtQixFQUFFLEVBQUU7b0JBQ3pDLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFRLENBQUM7b0JBQ2hDLGlCQUFVLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixJQUFJLENBQUMsRUFBRTs0QkFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFBOzRCQUN4QixPQUFNO3lCQUNUO3dCQUVELElBQUk7NEJBQ0EsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO3lCQUNoRDt3QkFDRCxPQUFPLENBQUMsRUFBRTt5QkFDVDt3QkFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFBO29CQUM1QixDQUFDLENBQUMsQ0FBQTtnQkFDTixDQUFDLENBQUMsQ0FBQTtnQkFFRixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtnQkFDbEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsT0FBbUIsRUFBRSxFQUFFO29CQUMzQyxJQUFJLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbkMsaUJBQVUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLEVBQUU7d0JBQ3pCLElBQUksQ0FBQyxFQUFFOzRCQUNILElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUE7NEJBQzFCLE9BQU07eUJBQ1Q7d0JBRUQsSUFBSTs0QkFDQSxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7eUJBQ2hEO3dCQUNELE9BQU8sQ0FBQyxFQUFFO3lCQUNUO3dCQUNELElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUE7b0JBQzlCLENBQUMsQ0FBQyxDQUFBO2dCQUNOLENBQUMsQ0FBQyxDQUFBO2FBQ0w7WUFFRCxNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUE7WUFFckIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUc7Z0JBQy9CLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFBO2dCQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7YUFDOUI7WUFFRCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO0tBQUE7SUFFRCxhQUFhO0lBQ2IsV0FBVyxDQUFDLEtBQWEsRUFBRSxNQUFXLEVBQUUsT0FBdUM7UUFDM0Usd0JBQXdCO1FBQ3hCLFNBQVMsUUFBUSxDQUFDLElBQVk7WUFDMUIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvRSxJQUFJLEVBQUUsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQ3RDLE9BQU8sRUFBRSxDQUFBO2FBQ1o7WUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQTthQUN2QjtZQUVELE9BQU8sRUFBRSxDQUFBO1FBQ2IsQ0FBQztRQUVELElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDbEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUM7UUFDbEMsS0FBSyxJQUFJLEdBQUcsSUFBSSxNQUFNLEVBQUU7WUFDcEIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO2dCQUFFLFNBQVM7U0FDckM7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFBO0lBQ3BGLENBQUM7SUFFRCxVQUFVLENBQUMsSUFBbUI7UUFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7SUFDN0IsQ0FBQztJQUVELFdBQVcsQ0FBQyxRQUE0QixFQUFFLE1BQWE7UUFDbkQsSUFBSSxRQUFRLElBQUksU0FBUztZQUFFLE9BQU87UUFFbEMsS0FBSyxJQUFJLElBQUksSUFBSSxRQUFRLEVBQUU7WUFDdkIsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3pCLElBQUksR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO2dCQUFFLFNBQVE7WUFDekIsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUNsQixJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQyxlQUFlO2dCQUNmLE1BQU0sS0FBSyxDQUFDLFVBQVUsSUFBSSw0QkFBNEIsR0FBRyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUE7YUFDckU7aUJBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbkIsU0FBUzthQUNaO1lBRUQsSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNoQyxTQUFRO2FBQ1g7WUFFRCxTQUFTO1lBQ1QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2IsTUFBTSxLQUFLLENBQUMsVUFBVSxHQUFHLGVBQWUsR0FBRyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUE7YUFDdkQ7aUJBQ0k7Z0JBQ0QsUUFBUSxHQUFHLENBQUMsSUFBSSxFQUFFO29CQUNkLEtBQUssUUFBUTt3QkFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUFDLE1BQU07b0JBQ3hELEtBQUssU0FBUzt3QkFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUFDLE1BQU07b0JBQzFELEtBQUssVUFBVSxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsVUFBVSxHQUFHLGVBQWUsR0FBRyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7b0JBQ3RFLEtBQUssUUFBUTt3QkFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUM3Qyw0QkFBNEI7d0JBQzVCLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTs0QkFDckMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTt5QkFDdEM7d0JBQ0QsTUFBTTtvQkFDVixLQUFLLFFBQVE7d0JBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7d0JBQUMsTUFBTTtvQkFDdkUsS0FBSyxRQUFRO3dCQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQUMsTUFBTTtvQkFDeEQsS0FBSyxRQUFRO3dCQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQUMsTUFBTTtvQkFDeEQsT0FBTyxDQUFDLENBQUMsTUFBTTtpQkFDbEI7YUFDSjtTQUNKO0lBQ0wsQ0FBQztJQUVlLFVBQVUsQ0FBQyxPQUFtQjs7WUFDMUMsNkJBQTZCO1lBQzdCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNULG1CQUFtQjtnQkFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUE7Z0JBQzNCLE9BQU87YUFDVjtZQUVELElBQUk7Z0JBQ0EsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDN0MsSUFBSSxDQUFDLEdBQVEsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzdELElBQUksQ0FBQyxZQUFZLE9BQU8sRUFBRTtvQkFDdEIsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFBO2lCQUNkO2dCQUVELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksU0FBUyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxFQUFFO29CQUN6QyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtpQkFDM0M7cUJBQ0k7b0JBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO2lCQUMvQjthQUNKO1lBQ0QsT0FBTyxDQUFDLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLENBQUMsRUFBRTtvQkFDSixDQUFDLEdBQUc7d0JBQ0EsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDUixNQUFNLEVBQUUsc0JBQXNCO3FCQUNqQyxDQUFBO2lCQUNKO3FCQUNJLElBQUksQ0FBQyxZQUFZLEtBQUssRUFBRTtvQkFDekIsQ0FBQyxHQUFHO3dCQUNBLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ1IsTUFBTSxFQUFFLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUM7cUJBQ2hELENBQUE7aUJBQ0o7Z0JBRUQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQTthQUN2QztRQUNMLENBQUM7S0FBQTtJQUVlLFlBQVksQ0FBQyxPQUFtQjs7WUFDNUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxNQUFNO2dCQUFFLE9BQU87WUFFcEIsSUFBSTtnQkFDQSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO2dCQUM3QyxJQUFJLENBQUMsR0FBUSxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLFlBQVksT0FBTyxFQUFFO29CQUN0QixDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUE7aUJBQ2Q7YUFDSjtZQUNELE9BQU8sQ0FBQyxFQUFFO2dCQUNOLElBQUksQ0FBQyxDQUFDLEVBQUU7b0JBQ0osQ0FBQyxHQUFHO3dCQUNBLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ1IsTUFBTSxFQUFFLHNCQUFzQjtxQkFDakMsQ0FBQTtpQkFDSjtxQkFDSSxJQUFJLENBQUMsWUFBWSxLQUFLLEVBQUU7b0JBQ3pCLENBQUMsR0FBRzt3QkFDQSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNSLE1BQU0sRUFBRSxDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUssR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDO3FCQUNoRCxDQUFBO2lCQUNKO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQyxDQUFBO2FBQ2xDO1FBQ0wsQ0FBQztLQUFBO0lBRVMsUUFBUSxDQUFDLE9BQW1CLEVBQUUsSUFBWSxFQUFFLEdBQVM7UUFDM0QsSUFBSSxJQUFJLElBQUksR0FBRyxFQUFFO1lBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUE7U0FDNUI7UUFFRCxTQUFTO1FBQ1QsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRTtZQUN2QixpQkFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsRUFBRTtnQkFDbEQsSUFBSSxDQUFDLEVBQUU7b0JBQ0gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUE7aUJBQ3pGO3FCQUNJO29CQUNELElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLE9BQU8sQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUE7aUJBQ3hGO1lBQ0wsQ0FBQyxDQUFDLENBQUE7U0FDTDthQUNJO1lBQ0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUE7U0FDekY7SUFDTCxDQUFDO0NBQ0o7QUFFRCxjQUFjO0FBQ2QsTUFBYSxTQUFTO0lBSWxCLE1BQU07SUFDTixNQUFNLENBQUMsT0FBTyxDQUFDLElBQVk7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLFVBQVUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQTtTQUMxRDtRQUVELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFlLENBQUE7SUFDL0MsQ0FBQztJQUVELE9BQU87SUFDUCxNQUFNLENBQUMsTUFBTSxDQUFDLElBQVksRUFBRSxHQUEyQjtRQUNuRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQ3hDLENBQUM7SUFFTyxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQVc7UUFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO1lBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFBO1FBQ3BELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztZQUFFLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDL0QsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO1lBQzFDLDBCQUEwQjtZQUMxQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksVUFBVSxDQUFBO1NBQ2pDO0lBQ0wsQ0FBQztJQUVEOzs7T0FHRztJQUNILE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBWSxFQUFFLEVBQWM7UUFDckMsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7WUFFM0IsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEIsV0FBVztnQkFDWCxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQWUsQ0FBQTtnQkFDN0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7Z0JBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO2FBQzdCO2lCQUNJO2dCQUNELElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7YUFDeEM7WUFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQTtZQUV6QixPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ2QsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLE9BQU8sQ0FBQTtnQkFDaEMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFBO1lBQ2xELENBQUMsQ0FBQTtTQUNKO2FBQ0k7WUFDRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTSxDQUFDLElBQUk7UUFDUCxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7Z0JBQUUsT0FBTztZQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ3pCLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUUsT0FBTyxDQUFDLEtBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQTtRQUNsRixDQUFDLENBQUE7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFjO1FBQ3ZCLE9BQU8sQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUFFO1lBQzdCLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxJQUFJLElBQUksUUFBUTtnQkFBRSxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ3BELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSztnQkFBRSxPQUFNO1lBQ3BDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUE7WUFDekIsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFBO1FBQ3RFLENBQUMsQ0FBQTtJQUNMLENBQUM7SUFFRDs7Ozs7TUFLRTtJQUNGLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBYSxFQUFFLEtBQXdCLEVBQUUsTUFBZTtRQUN6RSxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRTtZQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ3pCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDZixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUE7YUFDNUQ7UUFDTCxDQUFDLENBQUE7SUFDTCxDQUFDOztBQTdGTCw4QkE4RkM7QUE1RmtCLGlCQUFPLEdBQTRCLElBQUksR0FBRyxDQUFDIn0=